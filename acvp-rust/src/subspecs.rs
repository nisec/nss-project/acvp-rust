/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

pub mod bn;
pub mod ecdsa;
pub mod rsa;
pub mod sha;
pub mod symmetric;

use anyhow::Result;
use arbitrary::{Arbitrary, Unstructured};
use serde::{Deserialize, Deserializer, Serialize, Serializer};

/// signed hex string
#[derive(PartialEq, Eq, Clone, Default)]
pub struct HexString {
	pub data: Vec<u8>,
	pub pos: bool,
}

impl From<&HexString> for String {
	fn from(hexstring: &HexString) -> String {
		let sign = if hexstring.pos { "" } else { "-" };
		let hexdigits = hex::encode(&hexstring.data);
		format!("{sign}{hexdigits}")
	}
}

impl<'a> Arbitrary<'a> for HexString {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		Ok(HexString {
			data: Arbitrary::arbitrary(u)?,
			pos: Arbitrary::arbitrary(u)?,
		})
	}
}

fn arbitrary_sized_bytes(size: usize, u: &mut Unstructured) -> arbitrary::Result<Vec<u8>> {
	if let Some(bytes) = u.peek_bytes(size) {
		let v: Vec<_> = bytes.into();
		assert!(v.len() == size);
		u.bytes(size)?;
		Ok(v)
	} else {
		Err(arbitrary::Error::NotEnoughData)
	}
}

impl HexString {
	/// a shorcut to create a positive-only arbitrary
	pub fn positive_arbitrary(u: &mut Unstructured) -> arbitrary::Result<Self> {
		Self::restricted_arbitrary(None, None, Some(true), u)
	}

	/// optionally restrict size and positivity while consuming as much unstructured as needed
	pub fn restricted_arbitrary(min: Option<usize>, max: Option<usize>, positive: Option<bool>, u: &mut Unstructured) -> arbitrary::Result<Self> {
		let pos = positive.map(|b| Ok(b)).unwrap_or_else(|| bool::arbitrary(u))?;
		let real_min = min.unwrap_or(0);
		let real_max = max.unwrap_or_else(|| u.len());
		if real_min > real_max {
			return Err(arbitrary::Error::NotEnoughData);
		};
		let random_max = real_max - real_min;
		let size = real_min + if random_max == 0 { 0 } else { usize::arbitrary(u)? % random_max };
		let data = arbitrary_sized_bytes(size, u)?;
		assert!(data.len() >= real_min);
		assert!(data.len() <= real_max);
		Ok(HexString { data, pos })
	}

	pub fn from_unsigned(data: &[u8]) -> Self {
		HexString { pos: true, data: data.into() }
	}

	pub fn parse_string(value: &str) -> Result<Self> {
		let (pos, hexstr) = match value.get(0..1) {
			Some("+") => (true, &value[1..]),
			Some("-") => (false, &value[1..]),
			_ => (true, value),
		};
		let odd = hexstr.len() % 2 == 1;
		let pad = if odd { "0" } else { "" };
		let padded = format!("{pad}{hexstr}");

		Ok(HexString { data: hex::decode(padded)?, pos })
	}
}

struct HexStringVisitor;
impl<'de> serde::de::Visitor<'de> for HexStringVisitor {
	type Value = HexString;
	fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
		formatter.write_str("an optionally signed hex string")
	}

	fn visit_str<E>(self, value: &str) -> Result<Self::Value, E>
	where
		E: serde::de::Error,
	{
		HexString::parse_string(value).map_err(|e| E::custom(format!("Failed to parse hex: {e}")))
	}
}

impl<'de> Deserialize<'de> for HexString {
	fn deserialize<D>(deserializer: D) -> Result<HexString, D::Error>
	where
		D: Deserializer<'de>,
	{
		deserializer.deserialize_str(HexStringVisitor)
	}
}

impl Serialize for HexString {
	fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
	where
		S: Serializer,
	{
		let string: String = self.into();
		serializer.serialize_str(&string)
	}
}

impl std::fmt::Debug for HexString {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let string: String = self.into();
		write!(f, "{}", string)
	}
}

#[derive(clap::ValueEnum, Clone)]
pub enum Subspec {
	Sha,
	Symmetric,
	Rsa,
	Bn,
	Ecdsa,
}

pub trait VectorSet: Serialize + for<'de> serde::Deserialize<'de> + Clone + for<'a> Arbitrary<'a> {
	type TestGroup: TestGroup;
	type AnswerSet: AnswerSet;
	fn test_groups(&self) -> &[Self::TestGroup];
	fn test_groups_vec(&mut self) -> &mut Vec<Self::TestGroup>;
	fn vs_id(&self) -> u64;
	fn vs_id_mut(&mut self) -> &mut u64;
	fn subspec(&self) -> String;
	/// compare the immediate fields to possibly group the lower layers
	fn shallow_eq(&self, other: &Self) -> bool;
}

pub trait AnswerSet: Serialize + for<'de> serde::Deserialize<'de> + Clone {
	type AnswerGroup: AnswerGroup;
	fn answer_groups(&self) -> &[Self::AnswerGroup];
	fn answer_groups_vec(&mut self) -> &mut Vec<Self::AnswerGroup>;
	fn vs_id(&self) -> u64;
}

pub trait TestGroup: Serialize + for<'de> serde::Deserialize<'de> + Clone {
	type Test: Test;
	fn tg_id(&self) -> u64;
	fn tg_id_mut(&mut self) -> &mut u64;
	fn tests(&self) -> &[Self::Test];
	fn tests_vec(&mut self) -> &mut Vec<Self::Test>;
	/// compare the immediate fields to possibly group the lower layers
	fn shallow_eq(&self, other: &Self) -> bool;
}

pub trait Test: Serialize + for<'de> serde::Deserialize<'de> + Clone {
	fn tc_id(&self) -> u64;
	fn tc_id_mut(&mut self) -> &mut u64;
}

pub trait Answer: Serialize + for<'de> serde::Deserialize<'de> + Eq + Clone {
	fn tc_id(&self) -> u64;
}

pub trait AnswerGroup: Serialize + for<'de> serde::Deserialize<'de> + Clone {
	type Answer: Answer;
	fn tg_id(&self) -> u64;
	fn answers(&self) -> &[Self::Answer];
	fn answers_vec(&mut self) -> &mut Vec<Self::Answer>;
}
