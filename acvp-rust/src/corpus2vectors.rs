/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use acvp_rust::subspecs::*;
use anyhow::Result;
use arbitrary::Unstructured;
use clap::Parser;
use std::io::Read;

#[derive(clap::Parser)]
struct Options {
	/// obtain sample answers from the specified runner
	#[clap(short = 'a', long = "answers")]
	library_name: Option<String>,
	#[clap(short, long, default_value = "llvm-profdata")]
	profdata_command: String,
	#[clap(value_enum)]
	subspec: Subspec,
	corpus_directories: Vec<String>,
	/// time limit to execute a single testcase, after this it will be considered failed
	#[clap(short, long, default_value = "9000")]
	timeout: u64,
	/// name of the output file with profdata
	#[clap(short, long, default_value = "profdata")]
	output: String,
}

fn process_vector_sets<VectorSet: acvp_rust::subspecs::VectorSet>(options: &Options) -> Result<()> {
	let mut vss: Vec<VectorSet> = vec![];
	for corpus_directory in &options.corpus_directories {
		let mut entries: Vec<_> = std::fs::read_dir(corpus_directory)?.map(|e| e.unwrap().path()).collect();
		entries.sort();
		let mut tc_id: u64 = 0;
		for entry in entries {
			let mut vs_file = std::fs::File::open(&entry)?;
			let mut data = vec![];
			vs_file.read_to_end(&mut data)?;
			let e = Unstructured::new(&data).arbitrary();
			let mut vs: VectorSet = match e {
				Ok(vs) => vs,
				Err(arbitrary::Error::NotEnoughData) => {
					tracing::warn!("Not enough data for {:?}", entry);
					continue;
				}
				Err(arbitrary::Error::IncorrectFormat) => {
					tracing::warn!("Wrong format for {:?}", entry);
					continue;
				}
				Err(e) => return Err(e.into()),
			};
			if let Some(target_vs) = vss.iter_mut().find(|candidate| candidate.shallow_eq(&vs)) {
				let tgs = target_vs.test_groups_vec();
				if vs.test_groups_vec().is_empty() {
					continue;
				};
				let mut tg = vs.test_groups_vec().remove(0);

				// number tests uniquiely
				for test in tg.tests_vec().iter_mut() {
					*test.tc_id_mut() = tc_id;
					tc_id += 1;
				}
				if let Some(target_tg) = tgs.iter_mut().find(|candidate| candidate.shallow_eq(&tg)) {
					target_tg.tests_vec().append(tg.tests_vec())
				} else {
					*tg.tg_id_mut() = tgs.len() as u64;
					tgs.push(tg);
				}
			} else {
				*vs.vs_id_mut() = vss.len() as u64;
				for test_group in vs.test_groups_vec().iter_mut() {
					for test in test_group.tests_vec().iter_mut() {
						*test.tc_id_mut() = tc_id;
						tc_id += 1;
					}
				}
				vss.push(vs);
			}
		}
	}

	let version = acvp_rust::AcvVersion { acv_version: "1.0".into() };

	if let Some(library_name) = &options.library_name {
		let mut profiler = acvp_rust::Profiler::start(&options.profdata_command)?;
		let mut vector_sets = Vec::with_capacity(vss.len());
		let mut answer_sets = Vec::with_capacity(vss.len());
		for vs in vss {
			let answer_set = acvp_rust::run_vector_set(&vs, &mut profiler, library_name, options.timeout)?;
			vector_sets.push(vs);
			answer_sets.push(answer_set);
		}
		serde_json::to_writer(std::io::stdout(), &((version.clone(), vector_sets), (version, answer_sets)))?;
		let profdata = profiler.finalize()?;
		std::fs::copy(profdata, &options.output)?;
	} else {
		serde_json::to_writer(std::io::stdout(), &(version, vss))?
	};
	Ok(())
}

fn main() -> Result<()> {
	acvp_rust::ensure_tracing_subscriber();
	let options = Options::parse();
	match options.subspec {
		Subspec::Sha => process_vector_sets::<acvp_rust::subspecs::sha::VectorSet>(&options),
		Subspec::Rsa => process_vector_sets::<acvp_rust::subspecs::rsa::VectorSet>(&options),
		Subspec::Ecdsa => process_vector_sets::<acvp_rust::subspecs::ecdsa::VectorSet>(&options),
		Subspec::Symmetric => process_vector_sets::<acvp_rust::subspecs::symmetric::VectorSet>(&options),
		Subspec::Bn => process_vector_sets::<acvp_rust::subspecs::bn::VectorSet>(&options),
	}?;

	Ok(())
}
