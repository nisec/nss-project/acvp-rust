/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// https://pages.nist.gov/ACVP/draft-celi-acvp-rsa.html

use crate::subspecs;
use crate::subspecs::HexString;
use arbitrary::{Arbitrary, Unstructured};
use serde::{Deserialize, Serialize};
use std::ops::*;

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Default, Arbitrary, Clone, Copy)]
pub enum DigestAlgorithm {
	#[serde(alias = "SHA-1")]
	#[default]
	SHA1,
	#[serde(alias = "SHA2-224")]
	SHA2_224,
	#[serde(alias = "SHA2-256")]
	SHA2_256,
	#[serde(alias = "SHA2-384")]
	SHA2_384,
	#[serde(alias = "SHA2-512")]
	SHA2_512,
	#[serde(alias = "SHA2-512/224")]
	SHA2_512_224,
	#[serde(alias = "SHA2-512/256")]
	SHA2_512_256,
}

impl DigestAlgorithm {
	pub fn digest_len(&self) -> usize {
		match self {
			DigestAlgorithm::SHA1 => 20,
			DigestAlgorithm::SHA2_224 => 28,
			DigestAlgorithm::SHA2_256 => 32,
			DigestAlgorithm::SHA2_384 => 48,
			DigestAlgorithm::SHA2_512 => 64,
			DigestAlgorithm::SHA2_512_224 => 28,
			DigestAlgorithm::SHA2_512_256 => 32,
		}
	}
}

#[derive(Deserialize, Serialize, Clone, Debug, Arbitrary, PartialEq, Eq)]
pub enum Revision {
	#[serde(alias = "FIPS186-4")]
	Fips186_4,
	#[serde(alias = "FIPS186-5")]
	Fips186_5,
	#[serde(alias = "FIPS186-2")]
	Fips186_2,
	#[serde(alias = "1.0")]
	V1_0,
	#[serde(alias = "2.0")]
	V2_0,
	#[serde(alias = "Sp800-56Br2")]
	Sp800_56Br2,
}

/// non-standard
#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Debug, Arbitrary)]
#[serde(rename_all = "camelCase")]
pub struct PrivateKey {
	pub modulus: Option<HexString>,
	pub public_exponent: Option<HexString>,
	pub private_exponent: Option<HexString>,
	pub prime1: Option<HexString>,
	pub prime2: Option<HexString>,
	pub exponent1: Option<HexString>,
	pub exponent2: Option<HexString>,
	pub coefficient: Option<HexString>,
}

fn arbitrary_sane_private_key(modulus_len: usize, test: &mut Test, u: &mut Unstructured) -> arbitrary::Result<()> {
	let n = HexString::restricted_arbitrary(Some(modulus_len), Some(modulus_len), Some(true), u)?;
	let q = n.clone();
	let p = HexString::from_unsigned(&[1]);
	let d = HexString::restricted_arbitrary(Some(1), Some(n.data.len()), Some(true), u)?;
	test.n = Some(n);
	test.q = Some(q);
	test.p = Some(p);
	test.d = Some(d);
	Ok(())
}

fn arbitrary_private_key(test: &mut Test, u: &mut Unstructured) -> arbitrary::Result<()> {
	let q = HexString::restricted_arbitrary(Some(1), None, Some(true), u)?;
	let p = HexString::restricted_arbitrary(Some(1), Some(q.data.len()), Some(true), u)?;
	let d = HexString::restricted_arbitrary(Some(1), Some(q.data.len()), Some(true), u)?;
	let sane_modulus = bool::arbitrary(u)?;
	if sane_modulus {
		let q_bi = num_bigint::BigUint::from_bytes_le(&q.data);
		let p_bi = num_bigint::BigUint::from_bytes_le(&p.data);
		test.n = Some(HexString::from_unsigned(&q_bi.mul(p_bi).to_bytes_le()));
	} else {
		test.n = Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?);
	}
	test.q = Some(q);
	test.p = Some(p);
	test.d = Some(d);
	Ok(())
}

impl<'a> Arbitrary<'a> for VectorSet {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		let mut vs = VectorSet {
			vs_id: 0,
			algorithm: "RSA".into(),
			mode: Mode::SigVer,
			revision: Revision::arbitrary(u)?,
			test_groups: vec![],
			recover_signed_message: None,
		};

		let sig_type = SignatureType::arbitrary(u)?;
		vs.mode = u
			.choose(&[
				Mode::SigVer,
				Mode::SigGen,
				Mode::DecryptionPrimitive,
				Mode::EncryptionPrimitive,
				Mode::SignaturePrimitive,
				Mode::SignatureVerificationPrimitive,
				Mode::EncryptionOaep,
				Mode::DecryptionOaep,
			])? // Mode::KeyGen sometimes
			.clone();
		if vs.mode == Mode::SignatureVerificationPrimitive || vs.mode == Mode::SigVer {
			vs.recover_signed_message = Some(Arbitrary::arbitrary(u)?);
		};
		match (&vs.mode, sig_type) {
			(Mode::SigVer, SignatureType::Pkcs1v1_5) => {
				let hash_alg: DigestAlgorithm = Arbitrary::arbitrary(u)?;

				let sane_signature = bool::arbitrary(u)?;

				let modulus = HexString::positive_arbitrary(u)?;

				// sane signature?
				let clamps = if sane_signature { (Some(modulus.data.len()), Some(modulus.data.len())) } else { (None, None) };
				let mut signature = HexString::restricted_arbitrary(clamps.0, clamps.1, Some(true), u)?;
				if sane_signature && signature.data.len() > 1 {
					signature.data[0] = 0x00;
					signature.data[1] = 0;
				}
				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					hash_alg: Some(hash_alg),
					sig_type: Some(SignatureType::Pkcs1v1_5),
					test_type: TestType::GDT, // TODO other types
					n: Some(modulus.clone()),
					e: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				});

				let mut testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					signature: Some(signature),
					..Default::default()
				};
				let sane_key = bool::arbitrary(u)?;
				if sane_key {
					arbitrary_sane_private_key(hash_alg.digest_len(), &mut testcase, u)?;
				} else {
					arbitrary_private_key(&mut testcase, u)?;
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::SigVer, SignatureType::Pss) => {
				let hash_alg: DigestAlgorithm = Arbitrary::arbitrary(u)?;

				let sane_salt = bool::arbitrary(u)?;
				let sane_modulus = bool::arbitrary(u)?;
				let sane_signature = bool::arbitrary(u)?;

				// sane salt len?
				let salt_len = if sane_salt { u64::arbitrary(u)? % hash_alg.digest_len() as u64 } else { u64::arbitrary(u)? };

				// sane modulus?
				let modulus = if sane_modulus {
					let min_size = if let Some(min_size) = salt_len.checked_add(hash_alg.digest_len() as u64 + 2 /* +1 for possible leading zero */) {
						min_size
					} else {
						return Err(arbitrary::Error::IncorrectFormat);
					};
					let mut modulus = HexString::restricted_arbitrary(Some(min_size as usize), None, Some(true), u)?;
					if modulus.data[0] == 0 {
						modulus.data[0] = 1;
					};
					if modulus.data.len() > 0 {
						let len = modulus.data.len();
						modulus.data[len - 1] = 0xbc;
					};
					modulus
				} else {
					HexString::positive_arbitrary(u)?
				};

				// sane signature?
				let clamps = if sane_signature { (Some(modulus.data.len()), Some(modulus.data.len())) } else { (None, None) };
				let signature = HexString::restricted_arbitrary(clamps.0, clamps.1, Some(true), u)?;

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					hash_alg: Some(hash_alg),
					sig_type: Some(SignatureType::Pss),
					salt_len: Some(salt_len),
					test_type: TestType::GDT, // TODO other types
					mask_function: Some(u.choose(&[MaskFunction::MGF1, MaskFunction::SHAKE_128, MaskFunction::SHAKE_256])?.clone()),
					n: Some(modulus.clone()),
					e: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				});

				let mut testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					signature: Some(signature),
					..Default::default()
				};
				let sane_key = bool::arbitrary(u)?;
				if sane_key {
					arbitrary_sane_private_key(hash_alg.digest_len(), &mut testcase, u)?;
				} else {
					arbitrary_private_key(&mut testcase, u)?;
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::SignatureVerificationPrimitive, _) => {
				let sane_signature = bool::arbitrary(u)?;

				let modulus = HexString::positive_arbitrary(u)?;

				// sane signature?
				let clamps = if sane_signature { (Some(modulus.data.len()), Some(modulus.data.len())) } else { (None, None) };
				let signature = HexString::restricted_arbitrary(clamps.0, clamps.1, Some(true), u)?;

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					test_type: TestType::GDT,                      // TODO other types
					n: Some(modulus.clone()),
					e: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				});

				let testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					signature: Some(signature),
					..Default::default()
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::DecryptionPrimitive, _) => {
				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					test_type: TestType::GDT,                      // TODO other types
					..Default::default()
				});

				let mut testcase = Test {
					ciphertext: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};

				let sane_key = bool::arbitrary(u)?;
				if sane_key {
					arbitrary_sane_private_key(testcase.ciphertext.as_ref().unwrap().data.len(), &mut testcase, u)?;
				} else {
					arbitrary_private_key(&mut testcase, u)?;
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::EncryptionPrimitive, _) => {
				let modulus = HexString::positive_arbitrary(u)?;

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					test_type: TestType::GDT,                      // TODO other types
					n: Some(modulus.clone()),
					e: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				});

				let testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::DecryptionOaep, _) => {
				let hash_alg: DigestAlgorithm = Arbitrary::arbitrary(u)?;

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					test_type: TestType::GDT,                      // TODO other types
					hash_alg: Some(hash_alg),
					mask_function: Some(Arbitrary::arbitrary(u)?),
					..Default::default()
				});

				let mut testcase = Test {
					label: Some(HexString::positive_arbitrary(u)?),
					seed: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};

				let sane_key = bool::arbitrary(u)?;
				if sane_key {
					arbitrary_sane_private_key(hash_alg.digest_len() * 2 + 2, &mut testcase, u)?;
				} else {
					arbitrary_private_key(&mut testcase, u)?;
				};

				let modulus_len = testcase.n.as_ref().unwrap().data.len();

				let sane_ciphertext = bool::arbitrary(u)?;
				if sane_ciphertext {
					testcase.ciphertext = Some(HexString::restricted_arbitrary(Some(modulus_len), Some(modulus_len), Some(true), u)?);
				} else {
					testcase.ciphertext = Some(HexString::positive_arbitrary(u)?);
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::EncryptionOaep, _) => {
				let hash_alg: DigestAlgorithm = Arbitrary::arbitrary(u)?;

				let sane_modulus = bool::arbitrary(u)?;
				let modulus = if sane_modulus {
					HexString::restricted_arbitrary(Some(hash_alg.digest_len() * 2 + 2), None, Some(true), u)?
				} else {
					HexString::positive_arbitrary(u)?
				};

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?), // TODO: non-standard junk here
					test_type: TestType::GDT,                      // TODO other types
					n: Some(modulus.clone()),
					e: Some(HexString::positive_arbitrary(u)?),
					hash_alg: Some(hash_alg),
					mask_function: Arbitrary::arbitrary(u)?,

					..Default::default()
				});

				let testcase = Test {
					label: Some(HexString::positive_arbitrary(u)?),
					seed: Some(HexString::positive_arbitrary(u)?),
					message: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::SignaturePrimitive, _) => {
				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?),
					test_type: TestType::GDT, // TODO other types
					..Default::default()
				});
				let mut testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};
				arbitrary_private_key(&mut testcase, u)?;
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::SigGen, SignatureType::Pss) => {
				let hash_alg: DigestAlgorithm = Arbitrary::arbitrary(u)?;
				let own_salt = bool::arbitrary(u)?;

				let salt_len = if Arbitrary::arbitrary(u)? {
					u64::arbitrary(u)? % hash_alg.digest_len() as u64
				} else {
					u64::arbitrary(u)?
				};

				let salt = if own_salt {
					Some(HexString::restricted_arbitrary(Some(salt_len as usize), Some(salt_len as usize), Some(true), u)?)
				} else {
					None
				};

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?),
					hash_alg: Some(hash_alg),
					sig_type: Some(SignatureType::Pss),
					salt_len: Some(salt_len),
					salt,
					test_type: TestType::GDT, // TODO other types
					mask_function: Some(u.choose(&[MaskFunction::MGF1, MaskFunction::SHAKE_128, MaskFunction::SHAKE_256])?.clone()),
					..Default::default()
				});
				let mut testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};
				arbitrary_private_key(&mut testcase, u)?;
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::SigGen, SignatureType::Pkcs1v1_5) => {
				let hash_alg: DigestAlgorithm = Arbitrary::arbitrary(u)?;

				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?),
					hash_alg: Some(hash_alg),
					sig_type: Some(SignatureType::Pkcs1v1_5),
					test_type: TestType::GDT, // TODO other types
					..Default::default()
				});
				let mut testcase = Test {
					message: Some(HexString::positive_arbitrary(u)?),
					..Default::default()
				};
				arbitrary_private_key(&mut testcase, u)?;
				vs.test_groups[0].tests.push(testcase);
			}
			(Mode::KeyGen, _) => {
				vs.test_groups.push(TestGroup {
					modulo: Some(*u.choose(&[2048, 3072, 4096])?),
					e: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
					tests: vec![Default::default()],
					..Default::default()
				});
			}
			_ => (),
		};
		Ok(vs)
	}
}

impl subspecs::VectorSet for VectorSet {
	type TestGroup = TestGroup;
	type AnswerSet = AnswerSet;
	fn test_groups(&self) -> &[Self::TestGroup] {
		&self.test_groups
	}
	fn test_groups_vec(&mut self) -> &mut Vec<Self::TestGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
	fn vs_id_mut(&mut self) -> &mut u64 {
		&mut self.vs_id
	}
	fn subspec(&self) -> String {
		"rsa".into()
	}
	fn shallow_eq(&self, other: &VectorSet) -> bool {
		self.algorithm == other.algorithm && self.mode == other.mode && self.revision == other.revision
	}
}

impl subspecs::TestGroup for TestGroup {
	type Test = Test;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn tg_id_mut(&mut self) -> &mut u64 {
		&mut self.tg_id
	}
	fn tests(&self) -> &[Self::Test] {
		&self.tests
	}
	fn tests_vec(&mut self) -> &mut Vec<Self::Test> {
		&mut self.tests
	}
	fn shallow_eq(&self, other: &TestGroup) -> bool {
		self.test_type == other.test_type
			&& self.modulo == other.modulo
			&& self.hash_alg == other.hash_alg
			&& self.prime_test == other.prime_test
			&& self.rand_p_q == other.rand_p_q
			&& self.info_generated_by_server == other.info_generated_by_server
			&& self.key_format == other.key_format
			&& self.pub_exp == other.pub_exp
			&& self.sig_type == other.sig_type
			&& self.salt_len == other.salt_len
			&& self.salt == other.salt
			&& self.conformance == other.conformance
			&& self.mask_function == other.mask_function
			&& self.hash_pair == other.hash_pair
			&& self.n == other.n
			&& self.e == other.e
			&& self.total_failing_tests == other.total_failing_tests
			&& self.total_tests == other.total_tests
	}
}

impl subspecs::Test for Test {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
	fn tc_id_mut(&mut self) -> &mut u64 {
		&mut self.tc_id
	}
}

impl subspecs::AnswerSet for AnswerSet {
	type AnswerGroup = AnswerGroup;
	fn answer_groups(&self) -> &[Self::AnswerGroup] {
		&self.test_groups
	}

	fn answer_groups_vec(&mut self) -> &mut Vec<Self::AnswerGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
}

impl subspecs::AnswerGroup for AnswerGroup {
	type Answer = Answer;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn answers(&self) -> &[Self::Answer] {
		&self.tests
	}

	fn answers_vec(&mut self) -> &mut Vec<Self::Answer> {
		&mut self.tests
	}
}

impl subspecs::Answer for Answer {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Arbitrary)]
#[serde(rename_all = "camelCase")]
pub enum Mode {
	SigVer,
	SigGen,
	KeyGen,
	DecryptionPrimitive,
	EncryptionPrimitive,
	SignaturePrimitive,
	SignatureVerificationPrimitive,
	DecryptionOaep,
	EncryptionOaep,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct VectorSet {
	pub vs_id: u64,
	pub algorithm: String,
	pub mode: Mode,
	pub revision: Revision,
	pub test_groups: Vec<TestGroup>,
	pub recover_signed_message: Option<bool>,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Default, Debug, Arbitrary)]
pub enum TestType {
	AFT,
	#[default]
	GDT,
	KAT,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub enum KeyFormat {
	Standard,
	Crt,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub enum PubExpMode {
	Fixed,
	Random,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Arbitrary)]
#[serde(rename_all = "camelCase")]
pub enum SignatureType {
	#[serde(rename = "ansx9.31")]
	Ansx9_31,
	#[serde(rename = "pkcs1v1.5")]
	Pkcs1v1_5,
	Pss,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Arbitrary)]
#[allow(non_camel_case_types)]
pub enum MaskFunction {
	#[serde(rename = "mgf1")]
	MGF1,
	#[serde(rename = "shake-128")]
	SHAKE_128,
	#[serde(rename = "shake-256")]
	SHAKE_256,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TestGroup {
	pub tg_id: u64,
	pub tests: Vec<Test>,
	pub test_type: TestType,
	pub modulo: Option<u64>,
	pub hash_alg: Option<DigestAlgorithm>,
	pub prime_test: Option<String>,
	pub rand_p_q: Option<String>,
	pub info_generated_by_server: Option<bool>,
	pub key_format: Option<KeyFormat>,
	pub pub_exp: Option<PubExpMode>,

	// siggen
	pub sig_type: Option<SignatureType>,
	pub salt_len: Option<u64>,
	pub salt: Option<HexString>, // non-standard
	pub conformance: Option<String>,
	pub mask_function: Option<MaskFunction>,

	// sigver
	pub hash_pair: Option<Vec<(DigestAlgorithm, u64)>>,
	pub n: Option<HexString>,
	pub e: Option<HexString>,

	pub total_tests: Option<u64>,
	pub total_failing_tests: Option<u64>,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Test {
	pub tc_id: u64,

	// keygen
	pub e: Option<HexString>,
	pub seed: Option<HexString>,
	pub bitlens: Option<Vec<u64>>,
	pub x_p1: Option<HexString>,
	pub x_p2: Option<HexString>,
	pub x_p: Option<HexString>,
	pub x_q1: Option<HexString>,
	pub x_q2: Option<HexString>,
	pub x_q: Option<HexString>,
	pub p_rand: Option<HexString>,
	pub q_rand: Option<HexString>,

	// siggen
	pub salt_len: Option<u64>,
	pub random_value: Option<HexString>,
	pub random_value_len: Option<u64>,

	// sigver
	pub signature: Option<HexString>,

	// primitive
	pub n: Option<HexString>,
	pub d: Option<HexString>,
	pub dmp1: Option<HexString>,
	pub dmq1: Option<HexString>,
	pub iqmp: Option<HexString>,
	pub p: Option<HexString>,
	pub q: Option<HexString>,
	pub message: Option<HexString>,

	// dec primitive
	pub ciphertext: Option<HexString>,

	// non-standard
	pub private_key: Option<PrivateKey>,
	pub label: Option<HexString>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerSet {
	pub vs_id: u64,
	pub algorithm: String,
	pub revision: Revision,
	pub is_sample: bool,
	pub test_groups: Vec<AnswerGroup>,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerGroup {
	pub tg_id: u64,
	pub conformance: Option<String>,
	pub n: Option<HexString>,
	pub e: Option<HexString>,
	pub tests: Vec<Answer>,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, PartialEq, Eq, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Answer {
	pub tc_id: u64,

	pub e: Option<HexString>,
	pub test_passed: Option<bool>,
	pub bitlens: Option<Vec<u64>>,
	pub x_p1: Option<HexString>,
	pub x_p2: Option<HexString>,
	pub x_p: Option<HexString>,
	pub p: Option<HexString>,
	pub x_q1: Option<HexString>,
	pub x_q2: Option<HexString>,
	pub x_q: Option<HexString>,
	pub q: Option<HexString>,
	pub n: Option<HexString>,
	pub d: Option<HexString>,
	pub dmp1: Option<HexString>,
	pub dmq1: Option<HexString>,
	pub iqmp: Option<HexString>,

	// siggen
	pub random_value: Option<HexString>,
	pub random_value_len: Option<u64>,
	pub signature: Option<HexString>,

	// dec primitive
	pub plain_text: Option<HexString>,

	// non-standard
	pub message: Option<HexString>,
	pub ciphertext: Option<HexString>,
	pub label: Option<HexString>,
}

#[derive(Deserialize, Serialize, Clone, PartialEq, Eq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct MCTResult {
	pub md: HexString,
}
