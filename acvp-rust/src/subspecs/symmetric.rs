/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// https://pages.nist.gov/ACVP/draft-celi-acvp-symmetric.html

use crate::subspecs;
use crate::subspecs::HexString;
use arbitrary::{Arbitrary, Unstructured};
use serde::{Deserialize, Serialize};

impl<'a> Arbitrary<'a> for VectorSet {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		let mut vs = VectorSet {
			vs_id: 0,
			algorithm: "AES-GCM".into(), // TODO
			revision: "FIPS186-5".into(),
			..Default::default()
		};

		vs.test_groups.push(TestGroup {
			test_type: TestType::AFT,      // TODO other types
			direction: Direction::Encrypt, // TODO
			payload_len: Arbitrary::arbitrary(u)?,
			aad_len: Arbitrary::arbitrary(u)?,
			tag_len: Arbitrary::arbitrary(u)?,
			iv_len: Arbitrary::arbitrary(u)?,
			tests: vec![Arbitrary::arbitrary(u)?],
			..Default::default()
		});

		Ok(vs)
	}
}

impl subspecs::VectorSet for VectorSet {
	type AnswerSet = AnswerSet;
	type TestGroup = TestGroup;
	fn test_groups(&self) -> &[Self::TestGroup] {
		&self.test_groups
	}
	fn test_groups_vec(&mut self) -> &mut Vec<Self::TestGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
	fn vs_id_mut(&mut self) -> &mut u64 {
		&mut self.vs_id
	}
	fn subspec(&self) -> String {
		"symmetric".into()
	}
	fn shallow_eq(&self, other: &VectorSet) -> bool {
		self.algorithm == other.algorithm && self.revision == other.revision
	}
}

impl subspecs::TestGroup for TestGroup {
	type Test = Test;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn tg_id_mut(&mut self) -> &mut u64 {
		&mut self.tg_id
	}
	fn tests(&self) -> &[Self::Test] {
		&self.tests
	}
	fn tests_vec(&mut self) -> &mut Vec<Self::Test> {
		&mut self.tests
	}
	fn shallow_eq(&self, other: &TestGroup) -> bool {
		self.test_type == other.test_type
			&& self.direction == other.direction
			&& self.payload_len == other.payload_len
			&& self.aad_len == other.aad_len
			&& self.tag_len == other.tag_len
			&& self.iv_len == other.iv_len
	}
}

impl subspecs::Test for Test {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
	fn tc_id_mut(&mut self) -> &mut u64 {
		&mut self.tc_id
	}
}

impl subspecs::AnswerSet for AnswerSet {
	type AnswerGroup = AnswerGroup;
	fn answer_groups(&self) -> &[Self::AnswerGroup] {
		&self.test_groups
	}

	fn answer_groups_vec(&mut self) -> &mut Vec<Self::AnswerGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
}

impl subspecs::AnswerGroup for AnswerGroup {
	type Answer = Answer;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn answers(&self) -> &[Self::Answer] {
		&self.tests
	}

	fn answers_vec(&mut self) -> &mut Vec<Self::Answer> {
		&mut self.tests
	}
}

impl subspecs::Answer for Answer {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
}

// TODO: use this
// fn supported_version(algorithm_name: &str) -> &'static str {
// 	match algorithm_name {
// 		"ACVP-AES-ECB" => "1.0",
// 		"ACVP-AES-CBC" => "1.0",
// 		"ACVP-AES-CBC-CS1" => "1.0",
// 		"ACVP-AES-CBC-CS2" => "1.0",
// 		"ACVP-AES-CBC-CS3" => "1.0",
// 		"ACVP-AES-OFB" => "1.0",
// 		"ACVP-AES-CFB1" => "1.0",
// 		"ACVP-AES-CFB8" => "1.0",
// 		"ACVP-AES-CFB128" => "1.0",
// 		"ACVP-AES-CTR" => "1.0",
// 		"ACVP-AES-FF1" => "1.0",
// 		"ACVP-AES-FF3-1" => "1.0",
// 		"ACVP-AES-GCM" => "1.0",
// 		"ACVP-AES-GCM-SIV" => "1.0",
// 		"ACVP-AES-XPN" => "1.0",
// 		"ACVP-AES-CCM" => "1.0",
// 		"ACVP-AES-XTS" => "2.0",
// 		"ACVP-AES-KW" => "1.0",
// 		"ACVP-AES-KWP" => "1.0",
// 		"ACVP-TDES-ECB" => "1.0",
// 		"ACVP-TDES-CBC" => "1.0",
// 		"ACVP-TDES-CBCI" => "1.0",
// 		"ACVP-TDES-CFB1" => "1.0",
// 		"ACVP-TDES-CFB8" => "1.0",
// 		"ACVP-TDES-CFB64" => "1.0",
// 		"ACVP-TDES-CFBP1" => "1.0",
// 		"ACVP-TDES-CFBP8" => "1.0",
// 		"ACVP-TDES-CFBP64" => "1.0",
// 		"ACVP-TDES-OFB" => "1.0",
// 		"ACVP-TDES-OFBI" => "1.0",
// 		"ACVP-TDES-CTR" => "1.0",
// 		"ACVP-TDES-KW" => "1.0",
// 		_ => unimplemented!(),
// 	}
// }

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub enum Direction {
	#[default]
	Encrypt,
	Decrypt,
}

#[derive(Deserialize, Serialize, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct VectorSet {
	pub vs_id: u64,
	pub algorithm: String,
	pub revision: String,
	pub test_groups: Vec<TestGroup>,
}

#[derive(Deserialize, Serialize, Clone, PartialEq, Eq, Default)]
pub enum TestType {
	#[default]
	AFT,
	MCT,
}

#[derive(Deserialize, Serialize, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct TestGroup {
	pub tg_id: u64,
	pub test_type: TestType,
	pub direction: Direction,
	pub tests: Vec<Test>,
	pub payload_len: u64,
	pub aad_len: u64,
	pub tag_len: u64,
	pub iv_len: u64,
}

#[derive(Deserialize, Serialize, Clone, Default, Arbitrary)] // TODO: implement Arbitrary manually
#[serde(rename_all = "camelCase")]
pub struct Test {
	pub tc_id: u64,
	pub pt: Option<HexString>,
	pub aad: Option<HexString>,
	pub iv: Option<HexString>,
	pub key: Option<HexString>,
	pub tag: Option<HexString>,
	pub ct: Option<HexString>,
}

#[derive(Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct AnswerSet {
	pub vs_id: u64,
	pub algorithm: String,
	pub revision: String,
	pub is_sample: bool,
	pub test_groups: Vec<AnswerGroup>,
}

#[derive(Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct AnswerGroup {
	pub tg_id: u64,
	pub tests: Vec<Answer>,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Answer {
	pub tc_id: u64,
	pub key: Option<HexString>,
	pub aad: Option<HexString>,
	pub iv: Option<HexString>,
	pub ct: Option<HexString>,
	pub pt: Option<HexString>,
	pub tag: Option<HexString>,
	pub test_passed: Option<bool>,
	pub results_array: Option<Vec<MCTResult>>,
}

#[derive(Deserialize, Serialize, Clone, PartialEq, Eq, Debug)]
#[serde(rename_all = "camelCase")]
pub struct MCTResult {
	pub md: HexString,
}
