/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// https://pages.nist.gov/ACVP/draft-celi-acvp-ecdsa.html

use crate::subspecs;
use crate::subspecs::HexString;
use arbitrary::{Arbitrary, Unstructured};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Default, Arbitrary, Clone)]
pub enum DigestAlgorithm {
	#[serde(rename = "SHA-1")]
	#[default]
	SHA1,
	#[serde(rename = "SHA2-224")]
	SHA2_224,
	#[serde(rename = "SHA2-256")]
	SHA2_256,
	#[serde(rename = "SHA2-384")]
	SHA2_384,
	#[serde(rename = "SHA2-512")]
	SHA2_512,
	#[serde(rename = "SHA2-512/224")]
	SHA2_512_224,
	#[serde(rename = "SHA2-512/256")]
	SHA2_512_256,
	#[serde(rename = "SHA3-224")]
	SHA3_224,
	#[serde(rename = "SHA3-256")]
	SHA3_256,
	#[serde(rename = "SHA3-384")]
	SHA3_384,
	#[serde(rename = "SHA3-512")]
	SHA3_512,
}

impl<'a> Arbitrary<'a> for VectorSet {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		let mode = Mode::arbitrary(u)?;
		let mut vs = VectorSet {
			vs_id: 0,
			algorithm: "ECDSA".into(),
			mode: mode.clone(),
			revision: "FIPS186-5".into(),
			is_sample: false,
			test_groups: vec![],
		};
		vs.test_groups.push(TestGroup {
			hash_alg: Arbitrary::arbitrary(u)?,
			test_type: TestType::GDT, // TODO other types
			conformance: None,        // TODO
			curve: Arbitrary::arbitrary(u)?,
			component_test: false, // TODO
			..Default::default()
		});
		let testcase = match &mode {
			Mode::SigVer => Test {
				message: Some(HexString::positive_arbitrary(u)?),
				qx: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				qy: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				r: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				s: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				..Default::default()
			},
			Mode::SigGen => Test {
				message: Some(HexString::positive_arbitrary(u)?),
				// non-standard
				qx: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				qy: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				r: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				s: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				d_a: Some(HexString::restricted_arbitrary(Some(1), None, Some(true), u)?),
				..Default::default()
			},
			Mode::KeyGen => Default::default(),
			Mode::KeyVer => Default::default(),
		};
		vs.test_groups[0].tests.push(testcase);

		Ok(vs)
	}
}

impl subspecs::VectorSet for VectorSet {
	type TestGroup = TestGroup;
	type AnswerSet = AnswerSet;
	fn test_groups(&self) -> &[Self::TestGroup] {
		&self.test_groups
	}
	fn test_groups_vec(&mut self) -> &mut Vec<Self::TestGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
	fn vs_id_mut(&mut self) -> &mut u64 {
		&mut self.vs_id
	}
	fn subspec(&self) -> String {
		"ecdsa".into()
	}
	fn shallow_eq(&self, other: &VectorSet) -> bool {
		self.algorithm == other.algorithm && self.mode == other.mode && self.revision == other.revision
	}
}

impl subspecs::TestGroup for TestGroup {
	type Test = Test;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn tg_id_mut(&mut self) -> &mut u64 {
		&mut self.tg_id
	}
	fn tests(&self) -> &[Self::Test] {
		&self.tests
	}
	fn tests_vec(&mut self) -> &mut Vec<Self::Test> {
		&mut self.tests
	}
	fn shallow_eq(&self, other: &TestGroup) -> bool {
		self.test_type == other.test_type && self.hash_alg == other.hash_alg && self.conformance == other.conformance && self.curve == other.curve && self.component_test == other.component_test
	}
}

impl subspecs::Test for Test {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
	fn tc_id_mut(&mut self) -> &mut u64 {
		&mut self.tc_id
	}
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Default, Debug)]
pub enum TestType {
	AFT,
	#[default]
	GDT,
	KAT,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Default, Arbitrary)]
#[serde(rename_all = "camelCase")]
pub enum Mode {
	#[default]
	SigVer,
	SigGen,
	KeyGen,
	KeyVer,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct VectorSet {
	pub vs_id: u64,
	pub algorithm: String,
	pub mode: Mode,
	pub revision: String,
	pub is_sample: bool,
	pub test_groups: Vec<TestGroup>,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TestGroup {
	pub tg_id: u64,
	pub tests: Vec<Test>,
	pub test_type: TestType,
	pub hash_alg: DigestAlgorithm,
	pub conformance: Option<String>,
	pub component_test: bool,
	pub curve: Curve,
}
#[allow(non_camel_case_types)]
#[derive(Deserialize, Serialize, PartialEq, Eq, Clone, Debug, Arbitrary, Default)]
pub enum Curve {
	#[default]
	#[serde(rename = "P-224")]
	P224,
	#[serde(rename = "P-256")]
	P256,
	#[serde(rename = "P-384")]
	P384,
	#[serde(rename = "P-521")]
	P521,
	#[serde(rename = "B-233")]
	B233,
	#[serde(rename = "B-283")]
	B283,
	#[serde(rename = "B-409")]
	B409,
	#[serde(rename = "B-571")]
	B571,
	#[serde(rename = "K-233")]
	K233,
	#[serde(rename = "K-283")]
	K283,
	#[serde(rename = "K-409")]
	K409,
	#[serde(rename = "K-571")]
	K571,
}

impl subspecs::AnswerSet for AnswerSet {
	type AnswerGroup = AnswerGroup;
	fn answer_groups(&self) -> &[Self::AnswerGroup] {
		&self.test_groups
	}

	fn answer_groups_vec(&mut self) -> &mut Vec<Self::AnswerGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
}

impl subspecs::AnswerGroup for AnswerGroup {
	type Answer = Answer;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn answers(&self) -> &[Self::Answer] {
		&self.tests
	}

	fn answers_vec(&mut self) -> &mut Vec<Self::Answer> {
		&mut self.tests
	}
}

impl subspecs::Answer for Answer {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Test {
	pub tc_id: u64,

	// sigver
	pub qx: Option<HexString>,
	pub qy: Option<HexString>,
	pub r: Option<HexString>,
	pub s: Option<HexString>,

	// non-standard
	pub d_a: Option<HexString>,

	pub message: Option<HexString>,
}

#[serde_with::skip_serializing_none]
#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerSet {
	pub vs_id: u64,
	pub algorithm: String,
	pub revision: String,
	pub is_sample: bool,
	pub test_groups: Vec<AnswerGroup>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerGroup {
	pub tg_id: u64,
	pub tests: Vec<Answer>,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Debug, Clone, Default)]
#[serde(rename_all = "camelCase")]
pub struct Answer {
	pub tc_id: u64,
	pub test_passed: bool,
}
