/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// big number operations
// unpublished

use crate::subspecs;
use crate::subspecs::HexString;
use arbitrary::{Arbitrary, Unstructured};
use serde::{Deserialize, Serialize};

impl<'a> Arbitrary<'a> for Test {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		let operation_type = u.choose(&[0, 1, 2, 3])?;
		let mut non_zero_len = || HexString::restricted_arbitrary(Some(1), None, None, u);
		let operation = match operation_type {
			0 => Operation::InvMod {
				a: non_zero_len()?,
				modulo: non_zero_len()?,
			},
			1 => Operation::Add {
				a: non_zero_len()?,
				b: non_zero_len()?,
			},
			2 => Operation::ExpMod {
				a: non_zero_len()?,
				power: non_zero_len()?,
				modulo: non_zero_len()?,
			},
			3 => Operation::Gcd {
				a: non_zero_len()?,
				b: non_zero_len()?,
			},
			4 => Operation::Lcm {
				a: non_zero_len()?,
				b: non_zero_len()?,
			},
			_ => panic!("out of bounds chosen"),
		};
		Ok(Test { tc_id: 0, operation })
	}
}

impl subspecs::VectorSet for VectorSet {
	type TestGroup = TestGroup;
	type AnswerSet = AnswerSet;
	fn test_groups(&self) -> &[Self::TestGroup] {
		&self.test_groups
	}
	fn test_groups_vec(&mut self) -> &mut Vec<Self::TestGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
	fn vs_id_mut(&mut self) -> &mut u64 {
		&mut self.vs_id
	}
	fn subspec(&self) -> String {
		"bn".into()
	}

	fn shallow_eq(&self, other: &VectorSet) -> bool {
		self.revision == other.revision
	}
}

impl<'a> Arbitrary<'a> for VectorSet {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		let vs = VectorSet {
			vs_id: 0,
			revision: "1.0".into(),
			test_groups: vec![TestGroup {
				tg_id: 0,
				tests: vec![Test::arbitrary(u)?],
			}],
		};

		Ok(vs)
	}
}

impl VectorSet {
	pub fn dummy(tests: Vec<Test>) -> Self {
		VectorSet {
			vs_id: 0,
			revision: "1.0".into(),
			test_groups: vec![TestGroup { tg_id: 0, tests }],
		}
	}
}

impl Default for VectorSet {
	fn default() -> Self {
		VectorSet {
			vs_id: 0,
			revision: "1.0".into(),
			test_groups: vec![],
		}
	}
}

impl subspecs::TestGroup for TestGroup {
	type Test = Test;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn tg_id_mut(&mut self) -> &mut u64 {
		&mut self.tg_id
	}
	fn tests(&self) -> &[Self::Test] {
		&self.tests
	}
	fn tests_vec(&mut self) -> &mut Vec<Self::Test> {
		&mut self.tests
	}
	fn shallow_eq(&self, _other: &TestGroup) -> bool {
		true
	}
}

impl subspecs::Test for Test {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
	fn tc_id_mut(&mut self) -> &mut u64 {
		&mut self.tc_id
	}
}

impl subspecs::AnswerSet for AnswerSet {
	type AnswerGroup = AnswerGroup;
	fn answer_groups(&self) -> &[Self::AnswerGroup] {
		&self.answer_groups
	}

	fn answer_groups_vec(&mut self) -> &mut Vec<Self::AnswerGroup> {
		&mut self.answer_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
}

impl subspecs::AnswerGroup for AnswerGroup {
	type Answer = Answer;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn answers(&self) -> &[Self::Answer] {
		&self.answers
	}

	fn answers_vec(&mut self) -> &mut Vec<Self::Answer> {
		&mut self.answers
	}
}

impl subspecs::Answer for Answer {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct VectorSet {
	pub vs_id: u64,
	pub revision: String,
	pub test_groups: Vec<TestGroup>,
}

#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TestGroup {
	pub tg_id: u64,
	pub tests: Vec<Test>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Test {
	pub tc_id: u64,
	pub operation: Operation,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub enum Operation {
	InvMod { a: HexString, modulo: HexString },
	Add { a: HexString, b: HexString },
	ExpMod { a: HexString, power: HexString, modulo: HexString },
	Gcd { a: HexString, b: HexString },
	Lcm { a: HexString, b: HexString },
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub enum OperationAnswer {
	InvMod { result: Option<HexString> },
	Add { result: HexString },
	ExpMod { result: Option<HexString> },
	Gcd { result: HexString },
	Lcm { result: HexString },
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerSet {
	pub vs_id: u64,
	pub revision: String,
	pub is_sample: bool,
	pub answer_groups: Vec<AnswerGroup>,
}

#[derive(Deserialize, Serialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AnswerGroup {
	pub tg_id: u64,
	pub answers: Vec<Answer>,
}

#[derive(Deserialize, Serialize, PartialEq, Eq, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Answer {
	pub tc_id: u64,
	pub operation_answer: OperationAnswer,
}
