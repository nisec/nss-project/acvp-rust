/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// https://pages.nist.gov/ACVP/draft-celi-acvp-sha.html
use crate::subspecs;
use arbitrary::{Arbitrary, Unstructured};
use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug, PartialEq, Eq, Default, Arbitrary, Clone)]
pub enum Algorithm {
	#[serde(alias = "SHA-1")]
	#[default]
	SHA1,
	#[serde(alias = "SHA2-224")]
	SHA2_224,
	#[serde(alias = "SHA2-256")]
	SHA2_256,
	#[serde(alias = "SHA2-384")]
	SHA2_384,
	#[serde(alias = "SHA2-512")]
	SHA2_512,
	#[serde(alias = "SHA2-512/224")]
	SHA2_512_224,
	#[serde(alias = "SHA2-512/256")]
	SHA2_512_256,
}

impl<'a> Arbitrary<'a> for VectorSet {
	fn arbitrary(u: &mut Unstructured<'a>) -> arbitrary::Result<Self> {
		let mut vs = VectorSet {
			algorithm: Arbitrary::arbitrary(u)?,
			revision: "1.0".into(),
			..Default::default()
		};
		vs.test_groups.push(TestGroup {
			test_type: Arbitrary::arbitrary(u)?,
			..Default::default()
		});
		let msg: Vec<u8> = Arbitrary::arbitrary(u)?;
		let len = msg.len() as u64;
		vs.test_groups[0].tests.push(Test { msg, len, ..Default::default() });
		Ok(vs)
	}
}

impl subspecs::VectorSet for VectorSet {
	type TestGroup = TestGroup;
	type AnswerSet = AnswerSet;
	fn test_groups(&self) -> &[Self::TestGroup] {
		&self.test_groups
	}
	fn test_groups_vec(&mut self) -> &mut Vec<Self::TestGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
	fn vs_id_mut(&mut self) -> &mut u64 {
		&mut self.vs_id
	}
	fn subspec(&self) -> String {
		"sha".into()
	}

	fn shallow_eq(&self, other: &VectorSet) -> bool {
		self.algorithm == other.algorithm && self.revision == other.revision
	}
}

impl subspecs::TestGroup for TestGroup {
	type Test = Test;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn tg_id_mut(&mut self) -> &mut u64 {
		&mut self.tg_id
	}
	fn tests(&self) -> &[Self::Test] {
		&self.tests
	}
	fn tests_vec(&mut self) -> &mut Vec<Self::Test> {
		&mut self.tests
	}

	fn shallow_eq(&self, other: &TestGroup) -> bool {
		self.test_type == other.test_type
	}
}

impl subspecs::Test for Test {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
	fn tc_id_mut(&mut self) -> &mut u64 {
		&mut self.tc_id
	}
}

impl subspecs::AnswerSet for AnswerSet {
	type AnswerGroup = AnswerGroup;
	fn answer_groups(&self) -> &[Self::AnswerGroup] {
		&self.test_groups
	}

	fn answer_groups_vec(&mut self) -> &mut Vec<Self::AnswerGroup> {
		&mut self.test_groups
	}
	fn vs_id(&self) -> u64 {
		self.vs_id
	}
}

impl subspecs::AnswerGroup for AnswerGroup {
	type Answer = Answer;
	fn tg_id(&self) -> u64 {
		self.tg_id
	}
	fn answers(&self) -> &[Self::Answer] {
		&self.tests
	}

	fn answers_vec(&mut self) -> &mut Vec<Self::Answer> {
		&mut self.tests
	}
}

impl subspecs::Answer for Answer {
	fn tc_id(&self) -> u64 {
		self.tc_id
	}
}

#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct VectorSet {
	pub vs_id: u64,
	pub algorithm: Algorithm,
	pub revision: String,
	pub is_sample: bool,
	pub test_groups: Vec<TestGroup>,
}

#[derive(Deserialize, Arbitrary, Default, Serialize, Clone, PartialEq, Eq, Debug)]
pub enum TestType {
	#[default]
	AFT,
	MCT,
}

#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TestGroup {
	pub tg_id: u64,
	pub test_type: TestType,
	pub tests: Vec<Test>,
}

#[derive(Deserialize, Serialize, Clone, Default, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Test {
	pub tc_id: u64,
	#[serde(serialize_with = "hex::serde::serialize")]
	#[serde(deserialize_with = "hex::serde::deserialize")]
	pub msg: Vec<u8>,
	pub len: u64,
}

#[derive(Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct AnswerSet {
	pub vs_id: u64,
	pub algorithm: Algorithm,
	pub revision: String,
	pub is_sample: bool,
	pub test_groups: Vec<AnswerGroup>,
}

#[derive(Deserialize, Serialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct AnswerGroup {
	pub tg_id: u64,
	pub tests: Vec<Answer>,
}

#[derive(Deserialize, Serialize, PartialEq, Clone, Eq)]
#[serde(rename_all = "camelCase")]
pub struct Answer {
	pub tc_id: u64,
	pub md: Option<crate::subspecs::HexString>,
	pub results_array: Option<Vec<MCTResult>>,
}

#[derive(Deserialize, Clone, PartialEq, Eq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct MCTResult {
	pub md: crate::subspecs::HexString,
}
