/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

pub mod subspecs;

pub use serde_json;

use crate::subspecs::*;
use anyhow::Result;
use serde::{Deserialize, Serialize};
use std::io::{Read, Write};
use wait_timeout::ChildExt;

pub trait CryptographicLibrary: Sized {
	fn new() -> Result<Self>;

	fn sha(&mut self, vector_set: &subspecs::sha::VectorSet) -> Result<subspecs::sha::AnswerSet> {
		Ok(subspecs::sha::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: vec![],
		})
	}

	fn symmetric(&mut self, vector_set: &subspecs::symmetric::VectorSet) -> Result<subspecs::symmetric::AnswerSet> {
		Ok(subspecs::symmetric::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: vec![],
		})
	}

	fn rsa(&mut self, vector_set: &subspecs::rsa::VectorSet) -> Result<subspecs::rsa::AnswerSet> {
		Ok(subspecs::rsa::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: vec![],
		})
	}

	fn ecdsa(&mut self, vector_set: &subspecs::ecdsa::VectorSet) -> Result<subspecs::ecdsa::AnswerSet> {
		Ok(subspecs::ecdsa::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: vec![],
		})
	}

	fn bn(&mut self, vector_set: &subspecs::bn::VectorSet) -> Result<subspecs::bn::AnswerSet> {
		Ok(subspecs::bn::AnswerSet {
			vs_id: vector_set.vs_id,
			revision: vector_set.revision.clone(),
			is_sample: false,
			answer_groups: vec![],
		})
	}
}

#[derive(Deserialize, Clone, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AcvVersion {
	pub acv_version: String,
}

impl AcvVersion {
	pub fn verify(&self) -> bool {
		self.acv_version.as_str() == "1.0"
	}
}

pub struct KAT<KATVectorSet: crate::subspecs::VectorSet> {
	vector_sets: Vec<KATVectorSet>,
	answer_sets: Option<Vec<KATVectorSet::AnswerSet>>,
}

pub fn run_vector_set<VectorSet: crate::subspecs::VectorSet>(vector_set: &VectorSet, profiler: &mut Profiler, library_name: &str, timeout: u64) -> Result<VectorSet::AnswerSet> {
	let executable = format!("./target/debug/acvp-runner-{}", library_name);
	tracing::debug!("VS: {}", serde_json::to_string(vector_set)?);
	let (profile_file_name, mut command, out_path) = profiler.start_executable(&executable, &[&vector_set.subspec()])?;
	serde_json::to_writer(command.stdin.take().unwrap(), vector_set)?;

	let mut buf = vec![];
	match command.wait_timeout(std::time::Duration::from_secs(timeout))? {
		None => {
			command.kill()?;
			anyhow::bail!("Command timeout")
		}
		Some(status) => {
			if !status.success() {
				std::fs::File::open(out_path)?.read_to_end(&mut buf)?;
				anyhow::bail!("Command failed: {}", String::from_utf8(buf)?);
			} else {
				writeln!(profiler.profiles_list, "{}", profile_file_name)?;
			}
		}
	};

	std::fs::File::open(out_path)?.read_to_end(&mut buf)?;
	let out: VectorSet::AnswerSet = serde_json::from_reader(buf.as_slice())?;
	Ok(out)
}

impl<KATVectorSet: crate::subspecs::VectorSet> KAT<KATVectorSet> {
	pub fn deserialize(read: impl std::io::Read, vector_set_only: bool) -> Result<Self> {
		if vector_set_only {
			let (v_vs, vector_sets): (AcvVersion, Vec<KATVectorSet>) = serde_json::from_reader(read)?;
			if !v_vs.verify() {
				anyhow::bail!("ACV version mismatch");
			}
			Ok(KAT { vector_sets, answer_sets: None })
		} else {
			#[allow(clippy::type_complexity)]
			let ((v_vs, vector_sets), (v_ea, answer_sets)): ((AcvVersion, Vec<KATVectorSet>), (AcvVersion, Vec<KATVectorSet::AnswerSet>)) = serde_json::from_reader(read)?;
			if !v_ea.verify() || !v_vs.verify() {
				anyhow::bail!("ACV version mismatch");
			}
			Ok(KAT {
				vector_sets,
				answer_sets: Some(answer_sets),
			})
		}
	}

	pub fn run_stuff(&self, profiler: &mut Profiler, library_names: &[&str], vector_sets: &[u64], test_groups: &[u64], test_cases: &[u64]) -> Result<bool> {
		use std::collections::BTreeSet;
		let vector_sets: BTreeSet<_> = BTreeSet::from_iter(vector_sets);
		let test_groups: BTreeSet<_> = BTreeSet::from_iter(test_groups);
		let test_cases: BTreeSet<_> = BTreeSet::from_iter(test_cases);

		let mut answers: std::collections::HashMap<(u64, u64, u64), _> = Default::default();

		let mut this_is_reference = true;
		let mut reference_source = "n/a";

		if let Some(answer_sets) = &self.answer_sets {
			for answer_set in answer_sets {
				for tg in answer_set.answer_groups() {
					for e in tg.answers() {
						let key = (answer_set.vs_id(), tg.tg_id(), e.tc_id());
						assert!(answers.insert(key, e.clone()).is_none());
					}
				}
			}
			this_is_reference = false;
			reference_source = "answers";
		}

		let mut total = 0;
		let mut failed = 0;
		// let mut not_implemented = 0;

		for library_name in library_names {
			for vector_set in &self.vector_sets {
				if !vector_sets.is_empty() && !vector_sets.contains(&vector_set.vs_id()) {
					continue;
				};
				let mut alone_vector_set = vector_set.clone();

				for test_group in vector_set.test_groups() {
					if !test_groups.is_empty() && !test_groups.contains(&test_group.tg_id()) {
						continue;
					};
					let mut alone_test_group = test_group.clone();
					for test in test_group.tests() {
						if !test_cases.is_empty() && !test_cases.contains(&test.tc_id()) {
							continue;
						};
						total += 1;
						let key = (vector_set.vs_id(), test_group.tg_id(), test.tc_id());
						let key_hint = format!("-v {} -g {} -t {}", vector_set.vs_id(), test_group.tg_id(), test.tc_id());

						*alone_test_group.tests_vec() = vec![test.clone()];
						*alone_vector_set.test_groups_vec() = vec![alone_test_group.clone()];
						let out = run_vector_set(&alone_vector_set, profiler, library_name, 10);
						match out {
							Ok(answer_set) => {
								for answer_group in answer_set.answer_groups() {
									for answer in answer_group.answers() {
										if this_is_reference {
											assert!(answers.insert(key, answer.clone()).is_none());
										} else if let Some(right_answer) = answers.get(&key) {
											if answer != right_answer {
												tracing::warn!("Answer for test {} is wrong", key_hint);
												tracing::info!(
													"Right: {}, given (by {reference_source}): {}",
													serde_json::to_string_pretty(&right_answer)?,
													serde_json::to_string_pretty(answer)?
												);

												failed += 1;
											}
										} else {
											tracing::warn!("No answer for {key_hint}");
										}
									}
								}
							}
							Err(error) => {
								tracing::warn!("Test {} error: {:?}", key_hint, error);
								failed += 1;
							}
						}
					}
				}
			}
			if this_is_reference {
				this_is_reference = false;
				reference_source = library_name;
			}
		}

		if self.answer_sets.is_some() {
			if tracing::enabled!(tracing::Level::DEBUG) {
				tracing::debug!("keys {:?} not implemented", answers.keys().collect::<Vec<_>>());
			};
			// not_implemented += answers.len();
		};

		let (res, passfail) = if failed == 0 { (true, "passed") } else { (false, "failed") };
		eprintln!("Test vectors {passfail}: total {total}, failed: {failed}");

		Ok(res)
	}
}

pub struct Profiler {
	tempdir: tempfile::TempDir,
	profiles_list: tempfile::NamedTempFile,
	fileno: u64,
	profdata_command: String,
}

impl Profiler {
	pub fn start(profdata_command: &str) -> Result<Self> {
		let tempdir = tempfile::tempdir()?;
		let profiles_list = tempfile::NamedTempFile::new()?;
		let fileno = 0;
		Ok(Profiler {
			tempdir,
			profiles_list,
			fileno,
			profdata_command: profdata_command.into(),
		})
	}

	pub fn start_executable(&mut self, executable: &str, args: &[&str]) -> Result<(String, std::process::Child, tempfile::TempPath)> {
		let profile_file_name = format!("{}/profile{}.profraw", self.tempdir.path().to_str().unwrap(), self.fileno);
		self.fileno += 1;
		let outfile = tempfile::NamedTempFile::new()?;
		let out_path = outfile.into_temp_path();
		let outfile = std::fs::File::create(&out_path)?;
		let child = std::process::Command::new(executable)
			.args(args)
			.stdin(std::process::Stdio::piped())
			.stdout(outfile)
			.env("LLVM_PROFILE_FILE", &profile_file_name)
			.spawn()?;

		Ok((profile_file_name, child, out_path))
	}

	pub fn finalize(&self) -> Result<tempfile::TempPath> {
		let profdata = tempfile::NamedTempFile::new()?.into_temp_path();

		std::process::Command::new(&self.profdata_command)
			.args(["merge", "--input-files", self.profiles_list.path().to_str().unwrap(), "--sparse", "-o", profdata.to_str().unwrap()])
			.spawn()?
			.wait()?;
		Ok(profdata)
	}
}

pub fn ensure_tracing_subscriber() {
	let _ = tracing_subscriber::fmt()
		.with_writer(std::io::stderr)
		.with_env_filter(
			tracing_subscriber::filter::EnvFilter::builder()
				.with_default_directive(tracing_subscriber::filter::LevelFilter::WARN.into())
				.with_env_var("LOG_LEVEL")
				.from_env()
				.unwrap(),
		)
		.try_init();
}
