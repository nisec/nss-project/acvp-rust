/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use acvp_rust::subspecs::Subspec;
use acvp_rust::Profiler;
use anyhow::Result;
use clap::Parser;

/// run a test vector with known answers
#[derive(clap::Parser)]
struct Options {
	/// input filename
	kat: String,
	#[clap(value_enum)]
	subspec: acvp_rust::subspecs::Subspec,
	/// name of the cryptographic library to choose the runner. First one will be the reference.
	library_names: Vec<String>,
	/// name of the output file with profdata
	#[clap(short, long, default_value = "profdata")]
	output: String,
	/// custom llvm-profdata command to run
	#[clap(short, long, default_value = "llvm-profdata")]
	profdata_command: String,
	#[clap(long, short = 'v')]
	vector_sets: Vec<u64>,
	#[clap(long, short = 'g')]
	test_groups: Vec<u64>,
	#[clap(long, short = 't')]
	test_cases: Vec<u64>,
	/// the input is only the vector set
	#[clap(long, short = 'V')]
	vector_set_only: bool,
}

fn process_kat<VectorSet: acvp_rust::subspecs::VectorSet>(
	read: impl std::io::Read,
	mut profiler: Profiler,
	vector_set_only: bool,
	library_names: &[&str],
	vector_sets: &[u64],
	test_groups: &[u64],
	test_cases: &[u64],
) -> Result<(bool, tempfile::TempPath)> {
	let kat = acvp_rust::KAT::<VectorSet>::deserialize(read, vector_set_only)?;
	let result = kat.run_stuff(&mut profiler, library_names, vector_sets, test_groups, test_cases)?;
	Ok((result, profiler.finalize()?))
}

fn main() -> Result<()> {
	acvp_rust::ensure_tracing_subscriber();
	let options = Options::parse();
	let request = std::fs::File::open(options.kat)?;
	let profiler = Profiler::start(&options.profdata_command)?;
	let library_names: Vec<&str> = options.library_names.iter().map(|s| s.as_str()).collect();
	// TODO: generalize this
	let (result, profdata) = match options.subspec {
		Subspec::Sha => process_kat::<acvp_rust::subspecs::sha::VectorSet>(
			request,
			profiler,
			options.vector_set_only,
			&library_names,
			&options.vector_sets,
			&options.test_groups,
			&options.test_cases,
		),
		Subspec::Symmetric => process_kat::<acvp_rust::subspecs::symmetric::VectorSet>(
			request,
			profiler,
			options.vector_set_only,
			&library_names,
			&options.vector_sets,
			&options.test_groups,
			&options.test_cases,
		),
		Subspec::Rsa => process_kat::<acvp_rust::subspecs::rsa::VectorSet>(
			request,
			profiler,
			options.vector_set_only,
			&library_names,
			&options.vector_sets,
			&options.test_groups,
			&options.test_cases,
		),
		Subspec::Bn => process_kat::<acvp_rust::subspecs::bn::VectorSet>(
			request,
			profiler,
			options.vector_set_only,
			&library_names,
			&options.vector_sets,
			&options.test_groups,
			&options.test_cases,
		),
		Subspec::Ecdsa => process_kat::<acvp_rust::subspecs::ecdsa::VectorSet>(
			request,
			profiler,
			options.vector_set_only,
			&library_names,
			&options.vector_sets,
			&options.test_groups,
			&options.test_cases,
		),
	}?;

	std::fs::copy(profdata, options.output)?;
	if result {
		Ok(())
	} else {
		anyhow::bail!("Test vector failed")
	}
}
