/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use acvp_rust::subspecs;
use acvp_rust::serde_json;
use acvp_rust::CryptographicLibrary;

pub fn fuzz_aes_gcm_testcase(data: &[u8]) {
	acvp_rust::ensure_tracing_subscriber();
	let result: serde_json::Result<subspecs::symmetric::VectorSet> = serde_json::from_reader(data);
	match result {
		Err(sjson_error) => if sjson_error.is_syntax() || sjson_error.is_data() || sjson_error.is_eof() { () },
		Ok(vs) => {
			let mut lib = runner::Library::new().unwrap();
			lib.symmetric(&vs).unwrap();
		},
	};
}

pub fn fuzz_bn_testcase(vs: &subspecs::bn::VectorSet) {
	acvp_rust::ensure_tracing_subscriber();
	let mut lib = runner::Library::new().unwrap();
	lib.bn(&vs).unwrap();
}


pub fn fuzz_sha_testcase(vs: &subspecs::sha::VectorSet) {
	acvp_rust::ensure_tracing_subscriber();
	let mut lib = runner::Library::new().unwrap();
	lib.sha(&vs).unwrap();
}


pub fn fuzz_rsa_testcase(vs: &subspecs::rsa::VectorSet) {
	acvp_rust::ensure_tracing_subscriber();
	let mut lib = runner::Library::new().unwrap();
	lib.rsa(&vs).unwrap();
}

pub fn fuzz_ecdsa_testcase(vs: &subspecs::ecdsa::VectorSet) {
	acvp_rust::ensure_tracing_subscriber();
	let mut lib = runner::Library::new().unwrap();
	lib.ecdsa(&vs).unwrap();
}

