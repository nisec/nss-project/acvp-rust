/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#![no_main]
use libfuzzer_sys::{fuzz_target, fuzz_mutator};
use acvp_rust::subspecs::HexString;
use acvp_rust::subspecs::symmetric as sym;

fuzz_target!(|data: &[u8]| {
	acvp_rust_fuzz::fuzz_aes_gcm_testcase(data);
});
