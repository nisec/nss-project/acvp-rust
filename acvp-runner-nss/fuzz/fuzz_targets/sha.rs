/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

#![no_main]
use libfuzzer_sys::fuzz_target;
use acvp_rust::subspecs::sha;


fuzz_target!(|data: sha::VectorSet| {
    acvp_rust_fuzz::fuzz_sha_testcase(&data);
});
