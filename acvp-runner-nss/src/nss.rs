/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use anyhow::{bail, Result};
use std::ops::Div;

use crate::nss_sys;
use acvp_rust::subspecs;
use acvp_rust::subspecs::{HexString, VectorSet};

pub struct NSS;

pub fn init_nss() -> Result<()> {
	let ret = unsafe { nss_sys::NSS_NoDB_Init(std::ptr::null()) };
	if ret != 0 {
		bail!("NSS init failed");
	};
	Ok(())
}

pub fn deinit_nss() -> Result<()> {
	let ret = unsafe { nss_sys::NSS_Shutdown() };
	if ret != 0 {
		bail!("NSS deinit failed: {}", NSS::port_error());
	}
	Ok(())
}

// fn sec_item<T: Sized>() -> *mut nss_sys::SECItem {
// 	sec_item_size(std::mem::size_of::<T>())
// }

fn translate_symmetric_name(algorithm_name: &str) -> Option<u32> {
	let nss_name = match algorithm_name {
		"ACVP-AES-GCM" => nss_sys::CKM_AES_GCM,
		_ => return None,
	};
	Some(nss_name)
}

impl acvp_rust::CryptographicLibrary for NSS {
	fn new() -> Result<Self> {
		Ok(NSS)
	}

	fn sha(&mut self, vector_set: &subspecs::sha::VectorSet) -> Result<subspecs::sha::AnswerSet> {
		init_nss()?;
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = match test_group.test_type {
					subspecs::sha::TestType::AFT => self.sha_aft(&vector_set.algorithm, test)?,
					subspecs::sha::TestType::MCT => self.sha_mct(&vector_set.algorithm, test)?,
				};
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::sha::AnswerGroup {
				tg_id: test_group.tg_id,
				tests: answers,
			});
		}
		deinit_nss()?;
		Ok(subspecs::sha::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: answer_groups,
		})
	}

	fn symmetric(&mut self, vector_set: &subspecs::symmetric::VectorSet) -> Result<subspecs::symmetric::AnswerSet> {
		init_nss()?;
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = self.aes_gcm(&vector_set.algorithm, test, test_group)?;
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::symmetric::AnswerGroup {
				tg_id: test_group.tg_id,
				tests: answers,
			});
		}
		deinit_nss()?;
		Ok(subspecs::symmetric::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: answer_groups,
		})
	}

	fn rsa(&mut self, vector_set: &subspecs::rsa::VectorSet) -> Result<subspecs::rsa::AnswerSet> {
		init_nss()?;
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = self.rsa_tc(test, test_group, vector_set)?;
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::rsa::AnswerGroup {
				tg_id: test_group.tg_id,
				conformance: None, // TODO these
				e: test_group.e.clone(),
				n: test_group.n.clone(),
				tests: answers,
			});
		}
		deinit_nss()?;
		Ok(subspecs::rsa::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: answer_groups,
		})
	}

	fn ecdsa(&mut self, vector_set: &subspecs::ecdsa::VectorSet) -> Result<subspecs::ecdsa::AnswerSet> {
		init_nss()?;
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = self.ecdsa_tc(test, test_group, vector_set)?;
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::ecdsa::AnswerGroup {
				tg_id: test_group.tg_id,
				tests: answers,
			});
		}
		deinit_nss()?;
		Ok(subspecs::ecdsa::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: answer_groups,
		})
	}

	fn bn(&mut self, vector_set: &subspecs::bn::VectorSet) -> Result<subspecs::bn::AnswerSet> {
		init_nss()?;
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = self.bn_tc(test, test_group, vector_set)?;
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::bn::AnswerGroup { tg_id: test_group.tg_id, answers });
		}
		deinit_nss()?;

		Ok(subspecs::bn::AnswerSet {
			vs_id: vector_set.vs_id,
			revision: vector_set.revision.clone(),
			is_sample: false,
			answer_groups,
		})
	}
}

pub struct RSAPublicKey<'a> {
	pub inner: nss_sys::RSAPublicKey,
	#[allow(dead_code)]
	pub arena: &'a Arena,
}

impl<'a> RSAPublicKey<'a> {
	fn new(n: &[u8], e: &[u8], arena: &'a Arena) -> Self {
		let modulus = SecItem::new(n, arena);
		let public_exponent = SecItem::new(e, arena);
		let inner = unsafe {
			nss_sys::RSAPublicKey {
				arena: arena.inner,
				modulus: *modulus.inner,
				publicExponent: *public_exponent.inner,
			}
		};
		RSAPublicKey { inner, arena }
	}
}

pub struct ECDSAPublicKey<'a> {
	pub inner: nss_sys::ECPublicKey,
	#[allow(dead_code)]
	arena: &'a Arena,
}

pub struct ECDSAPrivateKey<'a> {
	pub inner: nss_sys::ECPrivateKey,
	#[allow(dead_code)]
	arena: &'a Arena,
}

fn ecdsa_params(curve: &subspecs::ecdsa::Curve, arena: &Arena) -> Result<Option<*mut nss_sys::ECParams>> {
	let curve_oid = match curve {
		subspecs::ecdsa::Curve::P224 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECP224R1,
		subspecs::ecdsa::Curve::P256 => nss_sys::SECOidTag_SEC_OID_ANSIX962_EC_PRIME256V1,
		subspecs::ecdsa::Curve::P384 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECP384R1,
		subspecs::ecdsa::Curve::P521 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECP521R1,
		subspecs::ecdsa::Curve::B233 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT233R1,
		subspecs::ecdsa::Curve::B283 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT283R1,
		subspecs::ecdsa::Curve::B409 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT409R1,
		subspecs::ecdsa::Curve::B571 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT571R1,

		subspecs::ecdsa::Curve::K233 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT233K1,
		subspecs::ecdsa::Curve::K283 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT283K1,
		subspecs::ecdsa::Curve::K409 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT409K1,
		subspecs::ecdsa::Curve::K571 => nss_sys::SECOidTag_SEC_OID_SECG_EC_SECT571K1,
	};

	let ecparams_der = unsafe {
		let oid_data = nss_sys::SECOID_FindOIDByTag(curve_oid);
		if oid_data.is_null() {
			anyhow::bail!("curve oid not found")
		};

		let params_len = 2 + (*oid_data).oid.len;
		let mut data: Vec<u8> = Vec::with_capacity(params_len as usize);

		data.push(nss_sys::SEC_ASN1_OBJECT_ID as u8);
		data.push((*oid_data).oid.len as u8);
		let oid = std::ptr::slice_from_raw_parts((*oid_data).oid.data, (*oid_data).oid.len as usize);
		data.extend_from_slice(&*oid);
		SecItem::new(&data, arena)
	};

	let mut ecparams = std::ptr::null_mut();
	let rv = unsafe { nss_sys::EC_DecodeParams(ecparams_der.inner, &mut ecparams) };
	if rv == nss_sys::_SECStatus_SECFailure {
		tracing::warn!("Failed to decode EC parameters, probably unsupported curve ({:?})", curve);
		return Ok(None);
	};
	Ok(Some(ecparams))
}

fn ecdsa_publicvalue(qx: &[u8], qy: &[u8]) -> Vec<u8> {
	let pv_len = qx.len() + qy.len() + 1;
	let mut public_value: Vec<u8> = Vec::with_capacity(pv_len);
	public_value.push(nss_sys::EC_POINT_FORM_UNCOMPRESSED as u8);
	public_value.extend_from_slice(qx);
	public_value.extend_from_slice(qy);
	public_value
}

impl<'a> ECDSAPrivateKey<'a> {
	fn new(qx: &[u8], qy: &[u8], private_value: &[u8], curve: &subspecs::ecdsa::Curve, arena: &'a Arena) -> Result<Option<Self>> {
		let ecparams = if let Some(ecparams) = ecdsa_params(curve, arena)? {
			ecparams
		} else {
			return Ok(None);
		};
		let public_value = SecItem::new(&ecdsa_publicvalue(qx, qy), arena);
		let private_value = SecItem::new(private_value, arena);
		let version = SecItem::new(&[0], arena);
		let inner = unsafe {
			nss_sys::ECPrivateKey {
				ecParams: *ecparams,
				publicValue: *public_value.inner,
				privateValue: *private_value.inner,
				version: *version.inner,
			}
		};
		Ok(Some(ECDSAPrivateKey { inner, arena }))
	}
}

impl<'a> Drop for ECDSAPrivateKey<'a> {
	fn drop(&mut self) {
		unsafe {
			nss_sys::PORT_FreeArena(self.inner.ecParams.arena, 1);
		};
	}
}

impl<'a> ECDSAPublicKey<'a> {
	fn new(qx: &[u8], qy: &[u8], curve: &subspecs::ecdsa::Curve, arena: &'a Arena) -> Result<Option<Self>> {
		let ecparams = if let Some(ecparams) = ecdsa_params(curve, arena)? {
			ecparams
		} else {
			return Ok(None);
		};
		let public_value = ecdsa_publicvalue(qx, qy);

		unsafe {
			let public_value = SecItem::new(&public_value, &arena);

			let inner = nss_sys::ECPublicKey {
				ecParams: *ecparams,
				publicValue: *public_value.inner,
			};

			Ok(Some(ECDSAPublicKey { inner, arena }))
		}
	}
}

impl<'a> Drop for ECDSAPublicKey<'a> {
	fn drop(&mut self) {
		unsafe {
			nss_sys::PORT_FreeArena(self.inner.ecParams.arena, 1);
		};
	}
}

pub struct SecItem<'a> {
	pub inner: *mut nss_sys::SECItem,
	#[allow(dead_code)]
	pub arena: &'a Arena,
}

impl<'a> SecItem<'a> {
	pub fn data_mut(&mut self) -> &mut [u8] {
		unsafe { std::slice::from_raw_parts_mut((*self.inner).data, (*self.inner).len as usize) }
	}

	pub fn new(slice: &[u8], arena: &'a Arena) -> Self {
		let inner = unsafe {
			let inner = nss_sys::SECITEM_AllocItem(arena.inner, std::ptr::null_mut(), slice.len() as u32);
			std::ptr::copy(slice.as_ptr(), (*inner).data, (*inner).len as usize);
			inner
		};

		Self { inner, arena }
	}
}

pub struct MpInt {
	pub inner: *mut nss_sys::mp_int,
}

impl Default for MpInt {
	fn default() -> Self {
		unsafe {
			let bx: Box<nss_sys::mp_int> = Box::new(std::mem::zeroed());
			let inner: *mut nss_sys::mp_int = Box::into_raw(bx);

			let result = nss_sys::mp_init(inner);
			assert!(result as u32 == nss_sys::MP_OKAY);
			MpInt { inner }
		}
	}
}

impl std::fmt::Display for MpInt {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
		let size = unsafe { nss_sys::mp_radix_size(self.inner, 10) } as usize;
		let mut str_vec = vec![0; size];
		unsafe { nss_sys::mp_toradix(self.inner, str_vec.as_mut_ptr(), 10) };
		write!(f, "{}", String::from_utf8(unsafe { std::mem::transmute(str_vec) }).unwrap())
	}
}

impl MpInt {
	pub fn check_err(err: nss_sys::mp_err) {
		assert!(err == nss_sys::MP_OKAY as i32)
	}

	pub fn mut_ptr(&mut self) -> *mut nss_sys::mp_int {
		self.inner
	}

	pub fn ptr(&self) -> *const nss_sys::mp_int {
		self.inner
	}

	pub fn read_signed_octets(slice: &[u8]) -> Self {
		let mp = Self::default();
		let result = unsafe { nss_sys::mp_read_raw(mp.inner, std::mem::transmute(slice.as_ptr()), slice.len() as i32) } as u32;
		assert!(result == nss_sys::MP_OKAY);
		mp
	}

	pub fn into_signed_octets(&self) -> Vec<u8> {
		let mut output: Vec<u8> = vec![];
		unsafe {
			let sign = (*self.inner).sign;
			(*self.inner).sign = 0;
			let size = nss_sys::mp_unsigned_octet_size(self.inner);
			output.resize(size as usize, 0);
			MpInt::check_err(nss_sys::mp_to_unsigned_octets(self.inner, output.as_mut_ptr(), size));
			let mut real_output = Vec::with_capacity(size as usize + 1);
			real_output.push(sign as u8);
			real_output.append(&mut output);
			(*self.inner).sign = sign;
			real_output
		}
	}
}

impl From<&subspecs::HexString> for MpInt {
	fn from(hexstring: &subspecs::HexString) -> Self {
		let string: String = hexstring.into();
		let newint = MpInt::default();
		let cstring = std::ffi::CString::new(string).unwrap();
		unsafe { MpInt::check_err(nss_sys::mp_read_radix(newint.inner, cstring.as_ptr(), 16)) };
		newint
	}
}

impl From<&MpInt> for subspecs::HexString {
	fn from(mpint: &MpInt) -> subspecs::HexString {
		let size = unsafe { nss_sys::mp_radix_size(mpint.inner, 16) } as usize;
		let mut str_vec = Vec::with_capacity(size);
		str_vec.resize(size, 1);
		let str_cstr = unsafe { std::ffi::CString::from_vec_unchecked(str_vec) };
		let str_cstr_ptr = str_cstr.into_raw();
		unsafe { nss_sys::mp_toradix(mpint.inner, str_cstr_ptr, 16) };
		let str_cstr = unsafe { std::ffi::CString::from_raw(str_cstr_ptr) };
		let string = str_cstr.into_string().unwrap();

		subspecs::HexString::parse_string(&string).unwrap()
	}
}

impl Drop for MpInt {
	fn drop(&mut self) {
		unsafe {
			nss_sys::mp_clear(self.inner);
			drop(Box::from_raw(self.inner))
		};
	}
}

struct RSAPrivateKey<'a> {
	inner: nss_sys::RSAPrivateKeyStr,
	#[allow(dead_code)]
	arena: &'a Arena,
}

pub struct Arena {
	pub inner: *mut nss_sys::PLArenaPool,
}

impl Arena {
	pub fn new() -> Self {
		Self {
			inner: unsafe { nss_sys::PORT_NewArena(1024) },
		}
	}

	pub unsafe fn allocate<A>(&self) -> *mut A {
		std::mem::transmute(nss_sys::PORT_ArenaZAlloc(self.inner, std::mem::size_of::<A>()))
	}
}

impl Drop for Arena {
	fn drop(&mut self) {
		unsafe {
			nss_sys::PORT_FreeArena(self.inner, 1);
		};
	}
}

impl<'a> RSAPrivateKey<'a> {
	fn from_subspec(testcase: &subspecs::rsa::Test, arena: &'a Arena) -> Option<Self> {
		unsafe {
			fn present_or_empty(data_option: &Option<HexString>) -> &[u8] {
				match data_option {
					Some(hs) => &hs.data,
					None => &[],
				}
			}
			let mut inner = nss_sys::RSAPrivateKeyStr {
				modulus: *SecItem::new(present_or_empty(&testcase.n), arena).inner,
				privateExponent: *SecItem::new(present_or_empty(&testcase.d), arena).inner,
				publicExponent: *SecItem::new(&[], arena).inner, // *SecItem::new(present_or_empty(&private_key.public_exponent), arena).inner,
				prime1: *SecItem::new(present_or_empty(&testcase.p), arena).inner,
				prime2: *SecItem::new(present_or_empty(&testcase.q), arena).inner,
				exponent1: *SecItem::new(present_or_empty(&testcase.dmp1), arena).inner,
				exponent2: *SecItem::new(present_or_empty(&testcase.dmq1), arena).inner,
				coefficient: *SecItem::new(present_or_empty(&testcase.iqmp), arena).inner,
				version: *SecItem::new(&[0], arena).inner,
				arena: arena.inner,
			};

			let rv = nss_sys::RSA_PopulatePrivateKey(&mut inner);
			if rv != 0 {
				tracing::debug!("RSAPopulatePrivateKey returned {}: {}", rv, NSS::port_error());
				return None;
			}

			Some(RSAPrivateKey { inner, arena })
		}
	}

	fn generate(key_size_in_bits: u64, public_exponent: &[u8], arena: &'a Arena) -> Result<Self> {
		unsafe {
			let raw_key = nss_sys::RSA_NewKey(key_size_in_bits as i32, SecItem::new(public_exponent, arena).inner);
			if raw_key == std::ptr::null_mut() {
				bail!("Failed to generate an RSA key: {}", NSS::port_error());
			};
			Ok(RSAPrivateKey { inner: *raw_key, arena })
		}
	}
}

impl NSS {
	fn port_error() -> String {
		let error = unsafe { nss_sys::PR_ErrorToString(nss_sys::PORT_GetError(), 0) };
		let serror = unsafe { std::ffi::CStr::from_ptr(error) };
		serror.to_string_lossy().into()
	}

	fn translate_sha_digest(algorithm: &subspecs::sha::Algorithm) -> Option<nss_sys::HASH_HashType> {
		let nss_name = match algorithm {
			subspecs::sha::Algorithm::SHA1 => nss_sys::HASH_HashType_HASH_AlgSHA1,
			subspecs::sha::Algorithm::SHA2_224 => nss_sys::HASH_HashType_HASH_AlgSHA224,
			subspecs::sha::Algorithm::SHA2_256 => nss_sys::HASH_HashType_HASH_AlgSHA256,
			subspecs::sha::Algorithm::SHA2_384 => nss_sys::HASH_HashType_HASH_AlgSHA384,
			subspecs::sha::Algorithm::SHA2_512 => nss_sys::HASH_HashType_HASH_AlgSHA512,
			subspecs::sha::Algorithm::SHA2_512_224 => return None,
			subspecs::sha::Algorithm::SHA2_512_256 => return None,
		};
		Some(nss_name)
	}

	fn translate_ecdsa_digest(algorithm: &subspecs::ecdsa::DigestAlgorithm) -> Option<nss_sys::HASH_HashType> {
		let nss_name = match algorithm {
			subspecs::ecdsa::DigestAlgorithm::SHA1 => nss_sys::HASH_HashType_HASH_AlgSHA1,
			subspecs::ecdsa::DigestAlgorithm::SHA2_224 => nss_sys::HASH_HashType_HASH_AlgSHA224,
			subspecs::ecdsa::DigestAlgorithm::SHA2_256 => nss_sys::HASH_HashType_HASH_AlgSHA256,
			subspecs::ecdsa::DigestAlgorithm::SHA2_384 => nss_sys::HASH_HashType_HASH_AlgSHA384,
			subspecs::ecdsa::DigestAlgorithm::SHA2_512 => nss_sys::HASH_HashType_HASH_AlgSHA512,
			subspecs::ecdsa::DigestAlgorithm::SHA2_512_224 => return None,
			subspecs::ecdsa::DigestAlgorithm::SHA2_512_256 => return None,
			_ => return None,
		};
		Some(nss_name)
	}

	fn translate_rsa_digest(algorithm: &subspecs::rsa::DigestAlgorithm) -> Option<nss_sys::HASH_HashType> {
		let nss_name = match algorithm {
			subspecs::rsa::DigestAlgorithm::SHA1 => nss_sys::HASH_HashType_HASH_AlgSHA1,
			subspecs::rsa::DigestAlgorithm::SHA2_224 => nss_sys::HASH_HashType_HASH_AlgSHA224,
			subspecs::rsa::DigestAlgorithm::SHA2_256 => nss_sys::HASH_HashType_HASH_AlgSHA256,
			subspecs::rsa::DigestAlgorithm::SHA2_384 => nss_sys::HASH_HashType_HASH_AlgSHA384,
			subspecs::rsa::DigestAlgorithm::SHA2_512 => nss_sys::HASH_HashType_HASH_AlgSHA512,
			subspecs::rsa::DigestAlgorithm::SHA2_512_224 => return None,
			subspecs::rsa::DigestAlgorithm::SHA2_512_256 => return None,
		};
		Some(nss_name)
	}

	fn sha_aft(&self, algorithm: &subspecs::sha::Algorithm, aft_test: &subspecs::sha::Test) -> Result<Option<subspecs::sha::Answer>> {
		let mut dest = vec![];
		let nss_algorithm = match Self::translate_sha_digest(algorithm) {
			Some(nss_algorithm) => nss_algorithm,
			None => return Ok(None),
		};

		unsafe {
			let ctx = nss_sys::HASH_Create(nss_algorithm);
			if ctx.is_null() {
				bail!("HASH_Create failed");
			}
			nss_sys::HASH_Begin(ctx);

			nss_sys::HASH_Update(ctx, aft_test.msg.as_ptr(), aft_test.msg.len() as u32);
			let hash_size = nss_sys::HASH_ResultLenContext(ctx);
			dest.resize(hash_size as usize, 0);
			let mut a = 0;
			nss_sys::HASH_End(ctx, dest.as_mut_ptr(), &mut a, hash_size);
			nss_sys::HASH_Destroy(ctx);
		};
		Ok(Some(subspecs::sha::Answer {
			tc_id: aft_test.tc_id,
			md: Some(HexString::from_unsigned(&dest)),
			results_array: None,
		}))
	}

	fn sha_mct(&self, algorithm: &subspecs::sha::Algorithm, mct_test: &subspecs::sha::Test) -> Result<Option<subspecs::sha::Answer>> {
		let nss_algorithm = match Self::translate_sha_digest(algorithm) {
			Some(nss_algorithm) => nss_algorithm,
			None => return Ok(None),
		};

		let mut a = 0;
		let ctx = unsafe { nss_sys::HASH_Create(nss_algorithm) };
		let hash_size = unsafe { nss_sys::HASH_ResultLenContext(ctx) };

		let mut dest = vec![];
		dest.resize(hash_size as usize, 0);

		let mut results = Vec::new();
		let mut md: Vec<Vec<u8>> = Vec::with_capacity(1003);
		md.resize(1003, vec![]);
		let mut seed = mct_test.msg.clone();
		for _ in 0..100 {
			md[0] = seed.clone();
			md[1] = seed.clone();
			md[2] = seed.clone();
			for i in 3..1003 {
				let mut msg = Vec::with_capacity(md[i - 3].len() + md[i - 2].len() + md[i - 1].len());
				msg.extend_from_slice(&md[i - 3]);
				msg.extend_from_slice(&md[i - 2]);
				msg.extend_from_slice(&md[i - 1]);
				unsafe {
					nss_sys::HASH_Begin(ctx);
					nss_sys::HASH_Update(ctx, msg.as_ptr(), msg.len() as u32);
					nss_sys::HASH_End(ctx, dest.as_mut_ptr(), &mut a, hash_size);
				};
				md[i] = dest.clone();
			}
			results.push(subspecs::sha::MCTResult {
				md: HexString::from_unsigned(&md[1002]),
			});
			seed = md[1002].clone();
		}
		unsafe { nss_sys::HASH_Destroy(ctx) };
		Ok(Some(subspecs::sha::Answer {
			tc_id: mct_test.tc_id,
			results_array: Some(results),
			md: None,
		}))
	}

	fn aes_gcm(&self, algorithm_name: &str, testcase: &subspecs::symmetric::Test, test_group: &subspecs::symmetric::TestGroup) -> Result<Option<subspecs::symmetric::Answer>> {
		let nss_algorithm = match translate_symmetric_name(algorithm_name) {
			Some(nss_algorithm) => nss_algorithm,
			None => return Ok(None),
		};
		let slot = unsafe { nss_sys::PK11_GetInternalSlot() };
		if slot.is_null() {
			bail!("Error allocating slot");
		}

		let sec_params = unsafe { nss_sys::SECITEM_AllocItem(std::ptr::null_mut(), std::ptr::null_mut(), std::mem::size_of::<nss_sys::CK_NSS_GCM_PARAMS>() as u32) };
		if sec_params.is_null() {
			bail!("Error allocating parameters");
		}
		let gcm_params = unsafe { sec_params.as_ref().unwrap().data as *mut nss_sys::CK_NSS_GCM_PARAMS };
		let key = testcase.key.clone().unwrap().data;
		let keysec = unsafe { nss_sys::SECITEM_AllocItem(std::ptr::null_mut(), std::ptr::null_mut(), key.len() as u32) }; // TODO: factor out

		if keysec.is_null() {
			bail!("Error importing key");
		}

		unsafe { std::ptr::copy(key.as_ptr(), (*keysec).data, key.len()) };
		let pk11_key = unsafe {
			nss_sys::PK11_ImportSymKey(
				slot,
				nss_algorithm as u64,
				nss_sys::PK11Origin_PK11_OriginUnwrap,
				(nss_sys::CKA_DECRYPT | nss_sys::CKA_ENCRYPT).into(),
				keysec,
				std::ptr::null_mut(),
			)
		};

		let mut out = vec![];
		let tag_bytes = test_group.tag_len.div(8);

		let mut iv = testcase.iv.clone().unwrap().data;
		let mut aad = testcase.aad.clone().unwrap().data;

		unsafe {
			(*gcm_params).pIv = iv.as_mut_ptr();
			(*gcm_params).ulIvLen = iv.len() as u64;
			(*gcm_params).ulTagBits = test_group.tag_len;
			(*gcm_params).pAAD = aad.as_mut_ptr();
			(*gcm_params).ulAADLen = aad.len() as u64;
		};
		let result = if test_group.direction == subspecs::symmetric::Direction::Encrypt {
			let mut pt = testcase.pt.clone().unwrap().data;

			out.resize(pt.len() + tag_bytes as usize, 0);
			let mut out_len = 0;
			let out_len: *mut u32 = &mut out_len;
			let rv = unsafe {
				nss_sys::PK11_Encrypt(
					pk11_key,
					nss_sys::CKM_AES_GCM as u64,
					sec_params,
					out.as_mut_ptr(),
					out_len,
					pt.len() as u32 + tag_bytes as u32,
					pt.as_mut_ptr(),
					pt.len() as u32,
				)
			};
			if rv != 0 {
				Ok(Some(subspecs::symmetric::Answer {
					test_passed: Some(false),
					tc_id: testcase.tc_id,
					..Default::default()
				}))
			} else {
				let ct = out[0..pt.len()].to_vec();
				let tag = out[pt.len()..].to_vec();
				Ok(Some(subspecs::symmetric::Answer {
					tc_id: testcase.tc_id,
					ct: Some(HexString::from_unsigned(&ct)),
					tag: Some(HexString::from_unsigned(&tag)),
					..Default::default()
				}))
			}
		} else {
			let mut tag = testcase.tag.clone().unwrap().data;
			let mut ct = testcase.ct.clone().unwrap().data;
			ct.append(&mut tag);

			let mut out_len = 0;
			let out_len: *mut u32 = &mut out_len;
			out.resize(ct.len() - tag_bytes as usize, 0);
			let rv = unsafe {
				nss_sys::PK11_Decrypt(
					pk11_key,
					nss_sys::CKM_AES_GCM as u64,
					sec_params,
					out.as_mut_ptr(),
					out_len,
					out.len() as u32,
					ct.as_mut_ptr(),
					ct.len() as u32,
				)
			};
			if rv != 0 {
				Ok(Some(subspecs::symmetric::Answer {
					tc_id: testcase.tc_id,
					test_passed: Some(false),
					..Default::default()
				}))
			} else {
				let pt = out;
				Ok(Some(subspecs::symmetric::Answer {
					tc_id: testcase.tc_id,
					pt: Some(HexString::from_unsigned(&pt)),
					..Default::default()
				}))
			}
		};
		unsafe { nss_sys::SECITEM_FreeItem(keysec, 1) };
		unsafe { nss_sys::SECITEM_FreeItem(sec_params, 1) };
		unsafe { nss_sys::PK11_FreeSlot(slot) };
		unsafe { nss_sys::PK11_FreeSymKey(pk11_key) };
		result
	}

	pub fn bn_tc(&self, testcase: &subspecs::bn::Test, _test_group: &subspecs::bn::TestGroup, _vector_set: &subspecs::bn::VectorSet) -> Result<Option<subspecs::bn::Answer>> {
		fn check_err(err: nss_sys::mp_err) -> Result<bool> {
			if err == nss_sys::MP_MEM {
				bail!("NSS out of memory")
			} else if err >= nss_sys::MP_OKAY as i32 {
				Ok(true)
			} else if err == nss_sys::MP_RANGE {
				Ok(true)
			} else {
				Ok(false)
			}
		}

		match &testcase.operation {
			subspecs::bn::Operation::InvMod { a, modulo } => {
				let a_bn: MpInt = a.into();
				let modulo_bn: MpInt = modulo.into();

				let result = MpInt::default();
				let mp_err = unsafe { nss_sys::mp_invmod(a_bn.inner, modulo_bn.inner, result.inner) };
				let ok = check_err(mp_err)?;
				let result = if ok {
					Some((&result).into())
				} else if mp_err == nss_sys::MP_RANGE || mp_err == nss_sys::MP_UNDEF || mp_err == nss_sys::MP_BADARG {
					None
				} else {
					bail!("Unexpected NSS error {mp_err}")
				};
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::InvMod { result },
				}))
			}
			subspecs::bn::Operation::Add { a, b } => {
				let a_bn: MpInt = a.into();
				let b_bn: MpInt = b.into();

				let result = MpInt::default();
				let mp_err = unsafe { nss_sys::mp_add(a_bn.inner, b_bn.inner, result.inner) };
				if !check_err(mp_err)? {
					bail!("Unexpected NSS error {mp_err}")
				};
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::Add { result: (&result).into() },
				}))
			}
			subspecs::bn::Operation::ExpMod { a, modulo, power } => {
				let a_bn: MpInt = a.into();
				let modulo_bn: MpInt = modulo.into();
				let power_bn: MpInt = power.into();

				let result = MpInt::default();
				let mp_err = unsafe { nss_sys::mp_exptmod(a_bn.inner, power_bn.inner, modulo_bn.inner, result.inner) };
				let ok = check_err(mp_err)?;
				let result = if ok {
					Some((&result).into())
				} else if mp_err == nss_sys::MP_RANGE || mp_err == nss_sys::MP_UNDEF || mp_err == nss_sys::MP_BADARG {
					None
				} else {
					bail!("Unexpected NSS error {mp_err}")
				};
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::ExpMod { result },
				}))
			}
			subspecs::bn::Operation::Gcd { a, b } => {
				let a_bn: MpInt = a.into();
				let b_bn: MpInt = b.into();

				let result = MpInt::default();
				let mp_err = unsafe { nss_sys::mp_gcd(a_bn.inner, b_bn.inner, result.inner) };
				if !check_err(mp_err)? {
					bail!("Unexpected NSS error {mp_err}")
				};
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::Gcd { result: (&result).into() },
				}))
			}
			subspecs::bn::Operation::Lcm { a, b } => {
				let a_bn: MpInt = a.into();
				let b_bn: MpInt = b.into();

				let result = MpInt::default();
				let mp_err = unsafe { nss_sys::mp_lcm(a_bn.inner, b_bn.inner, result.inner) };
				if mp_err == nss_sys::MP_RANGE {
					return Ok(None);
				} else if !check_err(mp_err)? {
					bail!("Unexpected NSS error {mp_err}")
				};
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::Lcm { result: (&result).into() },
				}))
			}
		}
	}

	fn rsa_tc(&self, testcase: &subspecs::rsa::Test, test_group: &subspecs::rsa::TestGroup, vector_set: &subspecs::rsa::VectorSet) -> Result<Option<subspecs::rsa::Answer>> {
		fn hash_vec(input: &[u8], hash_alg: nss_sys::HASH_HashType) -> Result<Vec<u8>> {
			let mut hash = vec![];
			let mut a = 0;
			let ctx = unsafe { nss_sys::HASH_Create(hash_alg) };
			if ctx.is_null() {
				bail!("HASH_Create failed");
			}
			unsafe {
				nss_sys::HASH_Begin(ctx);
				let hash_size = nss_sys::HASH_ResultLenContext(ctx);
				hash.resize(hash_size as usize, 0);
				nss_sys::HASH_Update(ctx, input.as_ptr(), input.len() as u32);
				nss_sys::HASH_End(ctx, hash.as_mut_ptr(), &mut a, hash_size);
				nss_sys::HASH_Destroy(ctx);
			}

			Ok(hash)
		}

		if vector_set.mode == subspecs::rsa::Mode::SigVer {
			let arena = Arena::new();

			match test_group.sig_type.as_ref().unwrap() {
				subspecs::rsa::SignatureType::Pss => {
					if test_group.mask_function != Some(subspecs::rsa::MaskFunction::MGF1) {
						return Ok(None);
					};
					let hash_alg = match Self::translate_rsa_digest(test_group.hash_alg.as_ref().unwrap()) {
						Some(a) => a,
						None => {
							tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
							return Ok(None);
						}
					};

					let input = testcase.message.clone().unwrap().data;

					let hash = hash_vec(&input, hash_alg)?;
					let salt_len = test_group.salt_len.clone().unwrap();

					let n = test_group.n.clone().unwrap().data;
					let e = test_group.e.clone().unwrap().data;

					let signature = testcase.signature.clone().unwrap().data;

					let mut pubkey = RSAPublicKey::new(&n, &e, &arena);
					let rv = unsafe {
						nss_sys::RSA_CheckSignPSS(
							&mut pubkey.inner,
							hash_alg,
							hash_alg, // TODO: is it?
							salt_len as u32,
							signature.as_ptr(),
							signature.len() as u32,
							hash.as_ptr(),
							hash.len() as u32,
						)
					};
					let answer = subspecs::rsa::Answer {
						test_passed: Some(rv == 0),
						tc_id: testcase.tc_id,
						..Default::default()
					};
					Ok(Some(answer))
				}
				subspecs::rsa::SignatureType::Pkcs1v1_5 if vector_set.revision == subspecs::rsa::Revision::Fips186_4 => {
					let hash_alg = match Self::translate_rsa_digest(test_group.hash_alg.as_ref().unwrap()) {
						Some(a) => a,
						None => {
							tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
							return Ok(None);
						}
					};
					let input = testcase.message.clone().unwrap().data;

					let hash = hash_vec(&input, hash_alg)?;
					let n = test_group.n.clone().unwrap().data;
					let e = test_group.e.clone().unwrap().data;

					let signature = testcase.signature.clone().unwrap().data;
					let mut message = signature.clone();
					let mut pubkey = RSAPublicKey::new(&n, &e, &arena);
					let rv = if vector_set.recover_signed_message.clone().unwrap_or(false) {
						let mut message_len: u32 = 0;
						let rv = unsafe {
							nss_sys::RSA_CheckSignRecover(
								&mut pubkey.inner,
								message.as_mut_ptr(),
								&mut message_len,
								message.len() as u32,
								signature.as_ptr(),
								signature.len() as u32,
							)
						};
						message.resize(message_len as usize, 0);
						rv
					} else {
						unsafe { nss_sys::RSA_CheckSign(&mut pubkey.inner, signature.as_ptr(), signature.len() as u32, hash.as_ptr(), hash.len() as u32) }
					};
					if rv != 0 {
						tracing::debug!("Failed to verify the message: {} ({})", rv, NSS::port_error())
					}
					let message = if vector_set.recover_signed_message.clone().unwrap_or(false) { Some(message) } else { None };
					let answer = subspecs::rsa::Answer {
						test_passed: Some(rv == 0),
						tc_id: testcase.tc_id,
						message: message.map(|v| HexString::from_unsigned(&v)),
						..Default::default()
					};
					Ok(Some(answer))
				}
				_ => Ok(None),
			}
		} else if vector_set.mode == subspecs::rsa::Mode::SigGen {
			match test_group.sig_type.as_ref().unwrap() {
				subspecs::rsa::SignatureType::Pss => {
					let hash_alg = match Self::translate_rsa_digest(test_group.hash_alg.as_ref().unwrap()) {
						Some(a) => a,
						None => {
							tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
							return Ok(None);
						}
					};

					let input = testcase.message.clone().unwrap().data;

					let hash = hash_vec(&input, hash_alg)?;

					let salt_len = test_group.salt_len.clone().unwrap();

					let salt = test_group.salt.as_ref().map(|salt_hs| salt_hs.data.as_ptr()).unwrap_or_else(|| std::ptr::null());

					let mut output_len = 0;
					let mut sig = Vec::new();

					let rv = {
						let arena = Arena::new();
						let mut key = match RSAPrivateKey::from_subspec(testcase, &arena) {
							Some(key) => {
								tracing::debug!("Key successfully generated");
								key
							}
							None => {
								tracing::debug!("Not sufficient data for a key");
								return Ok(None);
							}
						};

						sig.resize(key.inner.modulus.len as usize, 0);

						unsafe {
							nss_sys::RSA_SignPSS(
								&mut key.inner,
								hash_alg,
								hash_alg,
								salt,
								salt_len as u32,
								sig.as_mut_ptr(),
								&mut output_len,
								sig.len() as u32,
								hash.as_ptr(),
								hash.len() as u32,
							)
						}
					};

					if rv != 0 {
						tracing::debug!("Failed to sign the message: {} ({})", rv, NSS::port_error())
					}

					let test_passed = rv == 0;
					let signature = if test_passed { Some(HexString::from_unsigned(&sig[0..output_len as usize])) } else { None };
					Ok(Some(subspecs::rsa::Answer {
						test_passed: Some(test_passed),
						tc_id: testcase.tc_id,
						signature,
						..Default::default()
					}))
				}
				subspecs::rsa::SignatureType::Pkcs1v1_5 if vector_set.revision == subspecs::rsa::Revision::Fips186_4 => {
					let hash_alg = match Self::translate_rsa_digest(test_group.hash_alg.as_ref().unwrap()) {
						Some(a) => a,
						None => {
							tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
							return Ok(None);
						}
					};

					let input = testcase.message.clone().unwrap().data;

					let hash = hash_vec(&input, hash_alg)?;

					let mut output_len = 0;
					let mut sig = Vec::new();

					let rv = {
						let arena = Arena::new();
						let mut key = match RSAPrivateKey::from_subspec(testcase, &arena) {
							Some(key) => {
								tracing::debug!("Key successfully generated");
								key
							}
							None => {
								tracing::debug!("Not sufficient data for a key");
								return Ok(None);
							}
						};

						sig.resize(key.inner.modulus.len as usize, 0);

						unsafe { nss_sys::RSA_Sign(&mut key.inner, sig.as_mut_ptr(), &mut output_len, sig.len() as u32, hash.as_ptr(), hash.len() as u32) }
					};

					if rv != 0 {
						tracing::debug!("Failed to sign the message: {} ({})", rv, NSS::port_error())
					}

					let test_passed = rv == 0;
					let signature = if test_passed { Some(HexString::from_unsigned(&sig[0..output_len as usize])) } else { None };
					Ok(Some(subspecs::rsa::Answer {
						test_passed: Some(test_passed),
						tc_id: testcase.tc_id,
						signature,
						..Default::default()
					}))
				}
				_ => Ok(None),
			}
		} else if vector_set.mode == subspecs::rsa::Mode::KeyGen {
			let arena = Arena::new();
			let key_size_in_bits = test_group.modulo.unwrap();
			let public_exponent = test_group.e.as_ref().unwrap();
			let key = RSAPrivateKey::generate(key_size_in_bits, &public_exponent.data, &arena);
			if let Err(err) = key {
				tracing::debug!("Error generating key {:?}", err);
				Ok(Some(subspecs::rsa::Answer {
					test_passed: Some(false),
					tc_id: testcase.tc_id,
					..Default::default()
				}))
			} else {
				Ok(Some(subspecs::rsa::Answer {
					test_passed: Some(true),
					tc_id: testcase.tc_id,
					..Default::default()
				}))
			}
		} else if vector_set.mode == subspecs::rsa::Mode::SignaturePrimitive {
			let input = testcase.message.clone().unwrap().data;

			let mut output_len = 0;
			let mut sig = Vec::new();

			let rv = {
				let arena = Arena::new();
				let mut key = match RSAPrivateKey::from_subspec(testcase, &arena) {
					Some(key) => {
						tracing::debug!("Key successfully generated");
						key
					}
					None => {
						tracing::debug!("Not sufficient data for a key");
						return Ok(None);
					}
				};

				sig.resize(key.inner.modulus.len as usize, 0);

				unsafe { nss_sys::RSA_SignRaw(&mut key.inner, sig.as_mut_ptr(), &mut output_len, sig.len() as u32, input.as_ptr(), input.len() as u32) }
			};

			if rv != 0 {
				tracing::debug!("Failed to sign the message: {} ({})", rv, NSS::port_error())
			}

			let test_passed = rv == 0;
			let signature = if test_passed { Some(HexString::from_unsigned(&sig[0..output_len as usize])) } else { None };
			Ok(Some(subspecs::rsa::Answer {
				test_passed: Some(test_passed),
				tc_id: testcase.tc_id,
				signature,
				..Default::default()
			}))
		} else if vector_set.mode == subspecs::rsa::Mode::DecryptionPrimitive {
			let input = testcase.ciphertext.clone().unwrap().data;

			let mut output_len = 0;
			let mut output = Vec::new();

			let rv = {
				let arena = Arena::new();
				let mut key = match RSAPrivateKey::from_subspec(testcase, &arena) {
					Some(key) => {
						tracing::debug!("Key successfully generated");
						key
					}
					None => {
						tracing::debug!("Not sufficient data for a key");
						return Ok(None);
					}
				};

				output.resize(key.inner.modulus.len as usize, 0);

				unsafe { nss_sys::RSA_DecryptRaw(&mut key.inner, output.as_mut_ptr(), &mut output_len, output.len() as u32, input.as_ptr(), input.len() as u32) }
			};

			if rv != 0 {
				tracing::debug!("Failed to decrypt the message: {} ({})", rv, NSS::port_error())
			}

			let test_passed = rv == 0;
			let plain_text = if test_passed { Some(HexString::from_unsigned(&output[0..output_len as usize])) } else { None };
			Ok(Some(subspecs::rsa::Answer {
				tc_id: testcase.tc_id,
				plain_text,
				..Default::default()
			}))
		} else if vector_set.mode == subspecs::rsa::Mode::EncryptionOaep {
			let input = testcase.message.clone().unwrap().data;

			if test_group.mask_function != Some(subspecs::rsa::MaskFunction::MGF1) {
				return Ok(None);
			};
			let hash_alg = match Self::translate_rsa_digest(test_group.hash_alg.as_ref().unwrap()) {
				Some(a) => a,
				None => {
					tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
					return Ok(None);
				}
			};

			let mut output_len = 0;
			let mut output = Vec::new();

			let rv = {
				let arena = Arena::new();
				let n = test_group.n.clone().unwrap().data;
				let e = test_group.e.clone().unwrap().data;
				let label_data = testcase.label.clone().unwrap().data;
				let label = if label_data.len() == 0 { std::ptr::null() } else { label_data.as_ptr() }; // TODO: factor out
				let mut seed_data = testcase.seed.clone().unwrap().data;
				if !seed_data.is_empty() {
					seed_data.resize(test_group.hash_alg.as_ref().unwrap().digest_len(), 0);
				};
				let seed = if seed_data.len() == 0 { std::ptr::null() } else { seed_data.as_ptr() };

				let mut pubkey = RSAPublicKey::new(&n, &e, &arena);

				output.resize(pubkey.inner.modulus.len as usize, 0);

				unsafe {
					nss_sys::RSA_EncryptOAEP(
						&mut pubkey.inner,
						hash_alg,
						hash_alg,
						label,
						label_data.len() as u32,
						seed,
						seed_data.len() as u32,
						output.as_mut_ptr(),
						&mut output_len,
						output.len() as u32,
						input.as_ptr(),
						input.len() as u32,
					)
				}
			};

			if rv != 0 {
				tracing::debug!("Failed to encrypt the message: {} ({})", rv, NSS::port_error())
			}

			let test_passed = rv == 0;

			let ciphertext = if test_passed { Some(HexString::from_unsigned(&output[0..output_len as usize])) } else { None };

			Ok(Some(subspecs::rsa::Answer {
				tc_id: testcase.tc_id,
				ciphertext,
				..Default::default()
			}))
		} else if vector_set.mode == subspecs::rsa::Mode::DecryptionOaep {
			let input = testcase.ciphertext.clone().unwrap().data;
			let mut label = testcase.label.clone().unwrap().data;

			if test_group.mask_function != Some(subspecs::rsa::MaskFunction::MGF1) {
				return Ok(None);
			};
			let hash_alg = match Self::translate_rsa_digest(test_group.hash_alg.as_ref().unwrap()) {
				Some(a) => a,
				None => {
					tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
					return Ok(None);
				}
			};

			let mut output_len = 0;
			let mut output = Vec::new();
			let rv = {
				let arena = Arena::new();
				let mut key = match RSAPrivateKey::from_subspec(testcase, &arena) {
					Some(key) => {
						tracing::debug!("Key successfully generated");
						key
					}
					None => {
						tracing::debug!("Not sufficient data for a key");
						return Ok(None);
					}
				};

				output.resize(key.inner.modulus.len as usize, 0);
				label.resize(key.inner.modulus.len as usize, 0);

				unsafe {
					nss_sys::RSA_DecryptOAEP(
						&mut key.inner,
						hash_alg,
						hash_alg,
						label.as_mut_ptr(),
						label.len() as u32,
						output.as_mut_ptr(),
						&mut output_len,
						output.len() as u32,
						input.as_ptr(),
						input.len() as u32,
					)
				}
			};

			if rv != 0 {
				tracing::debug!("Failed to decrypt the message: {} ({})", rv, NSS::port_error())
			}

			let test_passed = rv == 0;
			let plain_text = if test_passed { Some(HexString::from_unsigned(&output[0..output_len as usize])) } else { None };
			let label_answer = if test_passed { Some(HexString::from_unsigned(&label)) } else { None };
			Ok(Some(subspecs::rsa::Answer {
				tc_id: testcase.tc_id,
				plain_text,
				label: label_answer,
				..Default::default()
			}))
		} else if vector_set.mode == subspecs::rsa::Mode::SignatureVerificationPrimitive {
			let input = testcase.message.clone().unwrap_or_default().data;
			let signature = testcase.signature.clone().unwrap().data;
			let mut message = signature.clone();

			let rv = {
				let arena = Arena::new();
				let n = test_group.n.clone().unwrap().data;
				let e = test_group.e.clone().unwrap().data;

				let mut pubkey = RSAPublicKey::new(&n, &e, &arena);

				if vector_set.recover_signed_message.clone().unwrap_or(false) {
					let mut message_len: u32 = 0;
					let rv = unsafe {
						nss_sys::RSA_CheckSignRecoverRaw(
							&mut pubkey.inner,
							message.as_mut_ptr(),
							&mut message_len,
							message.len() as u32,
							signature.as_ptr(),
							signature.len() as u32,
						)
					};
					message.resize(message_len as usize, 0);
					rv
				} else {
					unsafe { nss_sys::RSA_CheckSignRaw(&mut pubkey.inner, signature.as_ptr(), signature.len() as u32, input.as_ptr(), input.len() as u32) }
				}
			};

			if rv != 0 {
				tracing::debug!("Failed to verify the signature: {} ({})", rv, NSS::port_error())
			}

			let test_passed = rv == 0;

			let message = if vector_set.recover_signed_message.clone().unwrap_or(false) { Some(message) } else { None };
			Ok(Some(subspecs::rsa::Answer {
				tc_id: testcase.tc_id,
				test_passed: Some(test_passed),
				message: message.map(|v| HexString::from_unsigned(&v)),
				..Default::default()
			}))
		} else if vector_set.mode == subspecs::rsa::Mode::EncryptionPrimitive {
			let input = testcase.message.clone().unwrap().data;

			let mut output_len = 0;
			let mut output = Vec::new();

			let rv = {
				let arena = Arena::new();
				let n = test_group.n.clone().unwrap().data;
				let e = test_group.e.clone().unwrap().data;

				let mut pubkey = RSAPublicKey::new(&n, &e, &arena);

				output.resize(pubkey.inner.modulus.len as usize, 0);

				unsafe { nss_sys::RSA_EncryptRaw(&mut pubkey.inner, output.as_mut_ptr(), &mut output_len, output.len() as u32, input.as_ptr(), input.len() as u32) }
			};

			if rv != 0 {
				tracing::debug!("Failed to encrypt the message: {} ({})", rv, NSS::port_error())
			}

			let test_passed = rv == 0;

			let message = if test_passed { Some(HexString::from_unsigned(&output[0..output_len as usize])) } else { None };

			Ok(Some(subspecs::rsa::Answer {
				tc_id: testcase.tc_id,
				message,
				..Default::default()
			}))
		} else {
			tracing::debug!(
				"mode {:?}, mask function {:?} and signature type {:?} is not supported",
				vector_set.mode,
				test_group.mask_function,
				test_group.sig_type
			);
			Ok(None)
		}
	}

	fn ecdsa_tc(&self, testcase: &subspecs::ecdsa::Test, test_group: &subspecs::ecdsa::TestGroup, vector_set: &subspecs::ecdsa::VectorSet) -> Result<Option<subspecs::ecdsa::Answer>> {
		fn hash_vec(input: &[u8], hash_alg: nss_sys::HASH_HashType) -> Result<Vec<u8>> {
			let mut hash = vec![];
			let mut a = 0;
			let ctx = unsafe { nss_sys::HASH_Create(hash_alg) };
			if ctx.is_null() {
				bail!("HASH_Create failed");
			};
			unsafe {
				nss_sys::HASH_Begin(ctx);
				let hash_size = nss_sys::HASH_ResultLenContext(ctx);
				hash.resize(hash_size as usize, 0);
				nss_sys::HASH_Update(ctx, input.as_ptr(), input.len() as u32);
				nss_sys::HASH_End(ctx, hash.as_mut_ptr(), &mut a, hash_size);
				nss_sys::HASH_Destroy(ctx);
			};

			Ok(hash)
		}

		let arena = Arena::new();

		if vector_set.mode == subspecs::ecdsa::Mode::SigVer {
			let hash_alg = match Self::translate_ecdsa_digest(&test_group.hash_alg) {
				Some(a) => a,
				None => {
					tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
					return Ok(None);
				}
			};

			let input = testcase.message.clone().unwrap().data;

			let hash = hash_vec(&input, hash_alg)?;

			let mut r = testcase.r.clone().unwrap().data;
			let mut s = testcase.s.clone().unwrap().data;

			r.append(&mut s);

			let signature = SecItem::new(&r, &arena);

			let digest = SecItem::new(&hash, &arena);

			let pubkey = ECDSAPublicKey::new(&testcase.qx.clone().unwrap().data, &testcase.qy.clone().unwrap().data, &test_group.curve, &arena)?;

			if let Some(mut pubkey) = pubkey {
				let rv = unsafe { nss_sys::ECDSA_VerifyDigest(&mut pubkey.inner, signature.inner, digest.inner) };
				let answer = subspecs::ecdsa::Answer {
					test_passed: rv == 0,
					tc_id: testcase.tc_id,
				};
				Ok(Some(answer))
			} else {
				Ok(None)
			}
		} else if vector_set.mode == subspecs::ecdsa::Mode::SigGen {
			let hash_alg = match Self::translate_ecdsa_digest(&test_group.hash_alg) {
				Some(a) => a,
				None => {
					tracing::debug!("Not implemented hash: {:?}", test_group.hash_alg);
					return Ok(None);
				}
			};

			let input = testcase.message.clone().unwrap().data;

			let hash = hash_vec(&input, hash_alg)?;

			let mut r = testcase.r.clone().unwrap().data;
			let mut s = testcase.s.clone().unwrap().data;

			r.append(&mut s);

			let signature = SecItem::new(&[0; 9000], &arena);

			let digest = SecItem::new(&hash, &arena);

			let privkey = ECDSAPrivateKey::new(
				&testcase.qx.clone().unwrap().data,
				&testcase.qy.clone().unwrap().data,
				&testcase.d_a.clone().unwrap().data,
				&test_group.curve,
				&arena,
			)?;

			if let Some(mut privkey) = privkey {
				let rv = unsafe { nss_sys::ECDSA_SignDigest(&mut privkey.inner, signature.inner, digest.inner) };
				let answer = subspecs::ecdsa::Answer {
					test_passed: rv == 0,
					tc_id: testcase.tc_id,
				};
				Ok(Some(answer))
			} else {
				Ok(None)
			}
		} else {
			tracing::debug!("mode {:?} is not supported", vector_set.mode,);
			Ok(None)
		}
	}
}
