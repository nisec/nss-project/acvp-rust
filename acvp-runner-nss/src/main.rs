/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use acvp_rust::subspecs::Subspec;
use acvp_rust::CryptographicLibrary;
use anyhow::Result;
use clap::Parser;

#[derive(clap::Parser)]
/// Run ACVP vector set
struct Options {
	#[clap(value_enum)]
	subspec: Subspec,
}

fn main() -> Result<()> {
	acvp_rust::ensure_tracing_subscriber();
	let options = Options::parse();
	let mut library = acvp_runner_nss::Library::new()?;

	match options.subspec {
		Subspec::Sha => {
			let test_vector = serde_json::from_reader(std::io::stdin())?;
			let result = library.sha(&test_vector)?;
			serde_json::to_writer(std::io::stdout(), &result)?;
		}
		Subspec::Symmetric => {
			let test_vector = serde_json::from_reader(std::io::stdin())?;
			let result = library.symmetric(&test_vector)?;
			serde_json::to_writer(std::io::stdout(), &result)?;
		}
		Subspec::Rsa => {
			let test_vector = serde_json::from_reader(std::io::stdin())?;
			let result = library.rsa(&test_vector)?;
			serde_json::to_writer(std::io::stdout(), &result)?;
		}
		Subspec::Bn => {
			let test_vector = serde_json::from_reader(std::io::stdin())?;
			let result = library.bn(&test_vector)?;
			serde_json::to_writer(std::io::stdout(), &result)?;
		}
		Subspec::Ecdsa => {
			let test_vector = serde_json::from_reader(std::io::stdin())?;
			let result = library.ecdsa(&test_vector)?;
			serde_json::to_writer(std::io::stdout(), &result)?;
		}
	};

	Ok(())
}
