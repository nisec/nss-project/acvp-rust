/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
	let nss_path = env::var("NSS_PATH").unwrap_or_else(|_| panic!("Please set NSS_PATH to installed NSS prefix"));
	let nss_sources_path = env::var("NSS_SOURCES_PATH").unwrap_or_else(|_| panic!("Please set NSS_SOURCES_PATH to path to NSS source code"));

	env::set_var(
		"CPATH",
		format!(
			"{src}/lib/freebl:{src}/cmd/lib:{src}/cpputil:{src}/lib/util:{src}/lib/freebl/mpi:{inc}/include/nss:{inc}/dist/public/nss:{inc}/dist/Debug/include/nspr:{src}/lib/cryptohi",
			src = nss_sources_path,
			inc = nss_path
		),
	);

	env::set_var("LIBRARY_PATH", format!("{nss_path}/dist/Debug/lib"));
	println!("cargo:rustc-link-arg=-Wl,-Bstatic");
	println!("cargo:rustc-link-search={nss_path}/dist/Debug/lib");
	let a_regex = regex::Regex::new(r"lib([^/]+)\.a$").unwrap();
	let library_blocklist: std::collections::HashSet<String> = ["freebl", "pk11wrap", "softokn"].into_iter().map(Into::into).collect();
	let libraries: Vec<String> = glob::glob(&format!("{nss_path}/dist/Debug/lib/*.a"))
		.unwrap()
		.map(|path| a_regex.captures(&path.unwrap().to_string_lossy()).unwrap().get(1).unwrap().as_str().into())
		.filter(|lib| !library_blocklist.contains(lib))
		.collect();
	for library in libraries {
		println!("cargo:rustc-link-lib=static={}", library);
		println!("cargo:rerun-if-changed={nss_path}/dist/Debug/lib/lib{library}.a");
	}

	// Tell cargo to invalidate the built crate whenever the wrapper changes
	println!("cargo:rerun-if-changed=wrapper.h");

	// The bindgen::Builder is the main entry point
	// to bindgen, and lets you build up options for
	// the resulting bindings.
	let bindings = bindgen::Builder::default()
		// The input header we would like to generate
		// bindings for.
		.header("wrapper.h")
		.layout_tests(false)
		.detect_include_paths(true)
		// Tell cargo to invalidate the built crate whenever any of the
		// included header files changed.
		.parse_callbacks(Box::new(bindgen::CargoCallbacks))
		.allowlist_function("HASH_.*")
		.allowlist_function("RSA_.*")
		.allowlist_function("SECU_.*")
		.allowlist_function("NSS_.*")
		.allowlist_function("EC.*")
		.allowlist_function("PK11_.*")
		.allowlist_function("mp_.*")
		.allowlist_function("rsa_.*")
		.allowlist_function("SECITEM_.*")
		.allowlist_function("PORT_.*")
		.allowlist_function("PR_.*")
		.allowlist_function("SECOID.*")
		.allowlist_type("ScopedPK11*")
		.allowlist_type("ScopedSEC*")
		.allowlist_type("mp_int*")
		.allowlist_type("HASH_HashType_.*")
		.allowlist_type("SEC_OID.*")
		.allowlist_type("CK_NSS_GCM_PARAMS")
		.allowlist_type("_SECStatus")
		.allowlist_var("CKM_.*")
		.allowlist_var("SEC.*")
		.allowlist_var("CKA_.*")
		.allowlist_var("MP_.*")
		.allowlist_var("EC_.*")
		.allowlist_var("SECOidTag_.*")
		// Finish the builder and generate the bindings.
		.generate()
		// Unwrap the Result and panic on failure.
		.expect("Unable to generate bindings");

	// Write the bindings to the $OUT_DIR/bindings.rs file.
	let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
	bindings.write_to_file(out_path.join("bindings.rs")).expect("Couldn't write bindings!");
}
