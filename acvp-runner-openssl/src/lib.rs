/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

use acvp_rust::subspecs::{self, *};
use anyhow::Result;
use openssl::bn::BigNum;

pub struct Openssl {
	bn_context: openssl::bn::BigNumContext,
}

impl acvp_rust::CryptographicLibrary for Openssl {
	fn new() -> Result<Self> {
		Ok(Openssl {
			bn_context: openssl::bn::BigNumContext::new()?,
		})
	}

	fn bn(&mut self, vector_set: &subspecs::bn::VectorSet) -> Result<subspecs::bn::AnswerSet> {
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = self.bn_tc(test, test_group, vector_set)?;
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::bn::AnswerGroup { tg_id: test_group.tg_id, answers });
		}

		Ok(subspecs::bn::AnswerSet {
			vs_id: vector_set.vs_id,
			revision: vector_set.revision.clone(),
			is_sample: false,
			answer_groups,
		})
	}

	fn rsa(&mut self, vector_set: &subspecs::rsa::VectorSet) -> Result<subspecs::rsa::AnswerSet> {
		let mut answer_groups = Vec::with_capacity(vector_set.test_groups().len());
		for test_group in vector_set.test_groups() {
			let mut answers = Vec::with_capacity(test_group.tests.len());
			for test in &test_group.tests {
				let answer = self.rsa_tc(test, test_group, vector_set)?;
				if let Some(answer) = answer {
					answers.push(answer);
				};
			}

			answer_groups.push(subspecs::rsa::AnswerGroup {
				tg_id: test_group.tg_id,
				conformance: None, // TODO these
				e: test_group.e.clone(),
				n: test_group.n.clone(),
				tests: answers,
			});
		}
		Ok(subspecs::rsa::AnswerSet {
			vs_id: vector_set.vs_id,
			algorithm: vector_set.algorithm.clone(),
			revision: vector_set.revision.clone(),
			is_sample: false,
			test_groups: answer_groups,
		})
	}
}

impl Openssl {
	fn hex_to_bignum(hexstring: &subspecs::HexString) -> Result<BigNum> {
		let string: String = hexstring.into();
		// openssl can't handle zero-length numbers
		let string = if string == "-" {
			"-00".into()
		} else if string == "" {
			"00".into()
		} else {
			string
		};
		BigNum::from_hex_str(&string).map_err(Into::into)
	}

	fn bignum_to_hex(bn: &BigNum) -> Result<HexString> {
		HexString::parse_string(&bn.to_hex_str()?).map_err(Into::into)
	}

	pub fn bn_tc(&mut self, testcase: &subspecs::bn::Test, _test_group: &subspecs::bn::TestGroup, _vector_set: &subspecs::bn::VectorSet) -> Result<Option<subspecs::bn::Answer>> {
		match &testcase.operation {
			subspecs::bn::Operation::InvMod { a, modulo } => {
				let a_bn = Self::hex_to_bignum(a)?;
				let modulo_bn = Self::hex_to_bignum(modulo)?;

				let mut result = BigNum::new()?;
				let result = match result.mod_inverse(&a_bn, &modulo_bn, &mut self.bn_context) {
					Ok(()) => Some(Self::bignum_to_hex(&result)?),
					Err(openssl_error_stack) => {
						tracing::debug!("Openssl error: {:?}", openssl_error_stack);
						None
					}
				};
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::InvMod { result },
				}))
			}

			subspecs::bn::Operation::Add { a, b } => {
				let a_bn = Self::hex_to_bignum(a)?;
				let b_bn = Self::hex_to_bignum(b)?;

				let mut result = BigNum::new()?;
				result.checked_add(&a_bn, &b_bn)?;
				Ok(Some(subspecs::bn::Answer {
					tc_id: testcase.tc_id,
					operation_answer: subspecs::bn::OperationAnswer::Add {
						result: Self::bignum_to_hex(&result)?,
					},
				}))
			}
			_ => Ok(None),
		}
	}

	fn openssl_digest_algorithm(algorithm: Option<subspecs::rsa::DigestAlgorithm>) -> Option<openssl::hash::MessageDigest> {
		match algorithm? {
			subspecs::rsa::DigestAlgorithm::SHA1 => Some(openssl::hash::MessageDigest::sha1()),
			subspecs::rsa::DigestAlgorithm::SHA2_224 => Some(openssl::hash::MessageDigest::sha224()),
			subspecs::rsa::DigestAlgorithm::SHA2_256 => Some(openssl::hash::MessageDigest::sha256()),
			subspecs::rsa::DigestAlgorithm::SHA2_384 => Some(openssl::hash::MessageDigest::sha384()),
			subspecs::rsa::DigestAlgorithm::SHA2_512 => Some(openssl::hash::MessageDigest::sha512()),
			subspecs::rsa::DigestAlgorithm::SHA2_512_224 => None,
			subspecs::rsa::DigestAlgorithm::SHA2_512_256 => None,
		}
	}

	fn openssl_private_key(test_group: &subspecs::rsa::TestGroup) -> Result<openssl::rsa::Rsa<openssl::pkey::Private>> {
		unimplemented!()
		// let n = Self::hex_to_bignum(&test_group.n.as_ref().unwrap())?;
		// let e = Self::hex_to_bignum(&test_group.e.as_ref().unwrap())?;
		// let d = Self::hex_to_bignum(&test_group.d.as_ref().unwrap())?;
		// let rsab =  openssl::rsa::RsaPrivateKeyBuilder::new(n,e,d)?;
		// Ok(rsab.build())
	}

	fn openssl_public_key(test_group: &subspecs::rsa::TestGroup) -> Result<openssl::rsa::Rsa<openssl::pkey::Public>> {
		let n = Self::hex_to_bignum(test_group.n.as_ref().unwrap())?;
		let e = Self::hex_to_bignum(test_group.e.as_ref().unwrap())?;
		let rsa = openssl::rsa::Rsa::from_public_components(n, e)?;
		Ok(rsa)
	}

	fn rsa_tc(&self, testcase: &subspecs::rsa::Test, test_group: &subspecs::rsa::TestGroup, vector_set: &subspecs::rsa::VectorSet) -> Result<Option<subspecs::rsa::Answer>> {
		match vector_set.mode {
			subspecs::rsa::Mode::SigVer => {
				let digest_algorithm = if let Some(algo) = Self::openssl_digest_algorithm(test_group.hash_alg.clone()) {
					algo
				} else {
					return Ok(None);
				};

				let key = Self::openssl_public_key(&test_group)?;

				let pkey = openssl::pkey::PKey::from_rsa(key)?;
				let mut verifier = openssl::sign::Verifier::new(digest_algorithm, &pkey)?;
				verifier.set_rsa_mgf1_md(digest_algorithm)?;
				// if let Some(salt_len) = test_group.salt_len {
				//
				// 	let len = salt_len.try_into().unwrap();
				// 	eprintln!("{len}");
				// 	verifier.set_rsa_pss_saltlen(openssl::sign::RsaPssSaltlen::custom(len))?;
				// };
				verifier.set_rsa_padding(openssl::rsa::Padding::PKCS1_PSS)?;
				let ok = verifier.verify_oneshot(&testcase.signature.as_ref().unwrap().data, &testcase.message.as_ref().unwrap().data)?;
				let answer = subspecs::rsa::Answer {
					test_passed: Some(ok),
					tc_id: testcase.tc_id,
					..Default::default()
				};
				Ok(Some(answer))
			}
			_ => Ok(None),
		}
	}
}

pub use Openssl as Library;
